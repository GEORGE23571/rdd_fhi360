package com.example.monitoreo.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.monitoreo.R;
import com.example.monitoreo.adapters.AdapterCentroSelect;
import com.example.monitoreo.modelos.Departamento;
import com.example.monitoreo.modelos.Establecimiento;
import com.example.monitoreo.modelos.Formulario;
import com.example.monitoreo.modelos.Jornada;
import com.example.monitoreo.modelos.Nivel;
import com.example.monitoreo.modelos.municipio;
import com.example.monitoreo.utils.commons;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.monitoreo.utils.OpenBlankForm.newFormOdk;
import static com.example.monitoreo.utils.catalogos.contentXMLDia_de_clases_3;
import static com.example.monitoreo.utils.catalogos.contentXMLstallings2019_rdd;
import static com.example.monitoreo.utils.commons.getShared;

public class busquedaCentro extends AppCompatActivity
{
    RecyclerView recyclerView;

    Map<String, String> departamentosMap;
    Map<String, String> municipiosMap;
    Map<String, String> nivelesMap;
    Map<String, String> jornadasMap;

    ArrayList<Establecimiento> listaEstablecimientos;

    Spinner departamentoSpinner;
    Spinner municipioSpinner;
    Spinner nivelSpinner;
    Spinner jornadaSpinner;


    String deptoSeleccted;
    String muniSelected;
    String nivelSelected;
    String jornadaSelect;

    String jrIdForm;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busqueda_centro);
        initComponents();
        jrIdForm = getIntent().getStringExtra("JrFormId");

    }

    private void initComponents(){
        initRecyclerView();
        setSpinners();
        setListeners();
        cleanSpinners(0);
        setDepartamentos();
    }

    private void initRecyclerView()
    {
        recyclerView = findViewById(R.id.recyclerView);
    }

    private void setSpinners()
    {
        departamentoSpinner = findViewById(R.id.departamentoSpinner);
        municipioSpinner = findViewById(R.id.municipioSpinner);
        nivelSpinner = findViewById(R.id.nivelSpinner);
        jornadaSpinner=findViewById(R.id.jornadaSpinner);
    }

    private void setListeners()
    {
        departamentoSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String selected = ((TextView) selectedItemView).getText().toString();
                if (!selected.equals("Departamento"))
                {
                    cleanSpinners(1);
                    deptoSeleccted=departamentosMap.get(selected);
                    muniSelected=null;
                    nivelSelected=null;
                    jornadaSelect=null;
                    setMunicipios( deptoSeleccted);
                    setLista( );
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView)
            {
                // your code here
            }

        });

        municipioSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id)
            {
                String selected = ((TextView) selectedItemView).getText().toString();
                if (!selected.equals("Municipio"))
                {
                    muniSelected = municipiosMap.get(selected);
                    nivelSelected=null;
                    jornadaSelect=null;
                    setLista();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView)
            {
                // your code here
            }

        });
        nivelSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id)
            {
                String selected = ((TextView) selectedItemView).getText().toString();
                if (!selected.equals("Nivel"))
                {
                    nivelSelected = nivelesMap.get(selected);
                    jornadaSelect=null;
                    setLista();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView)
            {
                // your code here
            }

        });
        jornadaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String selected = ((TextView) selectedItemView).getText().toString();
                if (!selected.equals("Jornada"))
                {
                    jornadaSelect=jornadasMap.get(selected);
                    setLista();
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
    }

    private void setLista()
    {
        listaEstablecimientos=commons.BD.getEstablecimiento(deptoSeleccted,muniSelected,nivelSelected, jornadaSelect);
        AdapterCentroSelect ca;
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        registerForContextMenu(recyclerView);
        ca = new AdapterCentroSelect(this,listaEstablecimientos,R.layout.cardview_establecimiento_select_api);
        ca.setOnItemClickListener(onItemClickListener);
        recyclerView.setAdapter(ca);
    }

    /*CLICK LISTENER*/
    AdapterCentroSelect.OnItemClickListener onItemClickListener = new AdapterCentroSelect.OnItemClickListener()
    {
        @Override
        public void onItemClick(View view, Establecimiento obj, int position) {
            addEstablecimientoToFavoritos(obj);
        }
    };

    private void addEstablecimientoToFavoritos(Establecimiento e)
    {

        String formularios = getShared(busquedaCentro.this, "ArrayFormsODK","");
        Gson gson = new Gson();
        ArrayList<Formulario> forms = gson.fromJson(formularios, new TypeToken<ArrayList<Formulario>>(){}.getType());
        for (Formulario f : forms)
        {
            if(jrIdForm.equals(f.getId_form()))
            {
                newFormOdk(busquedaCentro.this ,  e,jrIdForm,"0" ,f.getBody_xml());
                break;
            }
        }
        finish();
    }

    private void setDepartamentos()
    {
        ArrayList<Departamento> deptos = commons.BD.getDepartamento();
        departamentosMap = new HashMap<>();
        String deptosArreglo[] = new String[deptos.size() + 1];
        deptosArreglo[0] = "Departamento";
        for (int i = 0; i < deptos.size() ; i++)
        {
            departamentosMap.put(
                    deptos.get(i).getNom_depto(),
                    deptos.get(i).getId_depto()
            );
            deptosArreglo[i + 1] = deptos.get(i).getNom_depto();
        }
        /*meter al spinner*/
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.spinner_item_white_text, deptosArreglo);
        adapter.setDropDownViewResource(R.layout.code_spinner_layout);
        departamentoSpinner.setAdapter(adapter);

    }

    private void setMunicipios(String departamento)
    { //Llenar municipios
        ArrayList<municipio> munis =commons.BD.getMunicipio(departamento);
        municipiosMap = new HashMap<>();
        String munisArreglo[] = new String[munis.size() + 1];
        munisArreglo[0] = "Municipio";

        for (int i = 0; i < munis.size() ; i++)
        {
            /*meter al arreglo para tener diccionario*/
            municipio element = munis.get(i);
            municipiosMap.put(element.getMunicipio(),element.muni);
            System.out.println("   "+element.getMunicipio()+"   "+element.muni);
            munisArreglo[i + 1] = element.getMunicipio();
        }

        /*meter al spinner*/
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.spinner_item_white_text, munisArreglo);
        adapter.setDropDownViewResource(R.layout.code_spinner_layout);
        municipioSpinner.setAdapter(adapter);

        ArrayList<Nivel> niveles =commons.BD.getNivel(departamento);

        nivelesMap = new HashMap<>();
        String nivelesArreglo[] = new String[niveles.size() + 1];
        nivelesArreglo[0] = "Nivel";
        for (int i = 0; i < niveles.size(); i++)
        {
            /*meter al arreglo para tener diccionario*/
            nivelesMap.put(niveles.get(i).getNivel(),niveles.get(i).getId_nivel()+"");
            nivelesArreglo[i + 1] = niveles.get(i).getNivel();
        }
        /*meter al spinner*/
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.spinner_item_white_text, nivelesArreglo);
        adapter2.setDropDownViewResource(R.layout.code_spinner_layout);
        nivelSpinner.setAdapter(adapter2);

        ArrayList<Jornada> jornadas =commons.BD.getJornada(departamento);
        jornadasMap = new HashMap<>();
        String jornadasArreglo[] = new String[jornadas.size() + 1];
        jornadasArreglo[0] = "Jornada";
        for (int i = 0; i < jornadas.size(); i++) {
            /*meter al arreglo para tener diccionario*/
            jornadasMap.put(jornadas.get(i).getNombre(),jornadas.get(i).getId());
            jornadasArreglo[i + 1] = jornadas.get(i).getNombre();
        }
        /*meter al spinner*/
        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.spinner_item_white_text, jornadasArreglo);
        adapter3.setDropDownViewResource(R.layout.code_spinner_layout);
        jornadaSpinner.setAdapter(adapter3);
    }


    private void cleanSpinners(int option)
    {
        /*
         * option = 0; desde departamento
         * option = 1; desde municipio
         * option = 2; desde nivel
         */
        ArrayList<String> array;
        ArrayAdapter<String> adapter;

        switch(option){
            case 0:
                array = new ArrayList<>();
                array.add("Departamento");
                adapter = new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.spinner_item_white_text, array);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                departamentoSpinner.setAdapter(adapter);
            case 1:
                array = new ArrayList<>();
                array.add("Municipio");
                adapter = new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.spinner_item_white_text, array);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                municipioSpinner.setAdapter(adapter);
            case 2:
                array = new ArrayList<>();
                array.add("Nivel");
                adapter = new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.spinner_item_white_text, array);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                nivelSpinner.setAdapter(adapter);
            case 3:
                array = new ArrayList<>();
                array.add("Jornada");
                adapter = new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.spinner_item_white_text, array);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                jornadaSpinner.setAdapter(adapter);
        }
    }
}