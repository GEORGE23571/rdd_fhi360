package com.example.monitoreo.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.monitoreo.R;
import com.example.monitoreo.modelos.Establecimiento;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Stack;

import static androidx.core.content.PermissionChecker.checkSelfPermission;

public class commons
{
    public static final String RDDAPI_URL = "http://3.14.150.185:8080/RDD/wsApp/";
    //public static final String RDDAPI_URL = "http://192.168.1.7:8080/RDD/wsresources/wsApp/";
    public static Manejador_BD BD;
    public static RequestQueue  queue=null;

    public static Activity lastStep;
    public static Activity mainActivity ;
    public static ProgressDialog progressDialog;
    public static String user;

   public static String getUser(Context context)
   {
       user = getShared(context, "user","");
       return user;
   }
    public static void setQueue(Context activity)
    {
       if(queue==null)
       {
           queue = Volley.newRequestQueue(activity);
       }
    }

    public static void initBD( Context context)
    {
        try
        {
            BD = new Manejador_BD(context);
            BD.CrearDB();
            BD.openDataBase();
        } catch (Exception e)
        {
            System.out.println("error abriendo BD "+ e.toString());
        }
    }
    /*UTILITIES*/
    public static void startActivity(Context context, Class c)
    {
        Intent i = new Intent(context, c);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }


    public static boolean estaInstaladaAplicacion(String nombrePaquete, Context context)
    {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(nombrePaquete, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
    /*END UTILITIES*/

    /*ESTABLECIMIENTOS FAVORITOS*/
    private static ArrayList<Establecimiento> establecimientos_favoritos;


    public static ArrayList<Establecimiento> getEstablecimientosFavoritos(Context activity) {
        establecimientos_favoritos = new ArrayList<>();
        String favoritos = getShared(activity, FAVORITOS, null);
        if (favoritos == null || favoritos.equals("")) return null;

        try {
            JSONArray jsonEstablecimientos = new JSONArray(favoritos);
            int len = jsonEstablecimientos.length();
            for (int i = 0; i < len; i++) {
                JSONObject jsonEstablecimiento = jsonEstablecimientos.getJSONObject(i);
                establecimientos_favoritos.add(Establecimiento.newFromJSON(jsonEstablecimiento));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return establecimientos_favoritos;
    }
    public static void addEstablecimientoFavorito(Context activity, Establecimiento e) {
        String establecimientosString = "[]";
        String establecimientos = getShared(activity, FAVORITOS, null);
        if (establecimientos == null || establecimientos.equals("")) {
            establecimientosString = "[" + e.toJSONString() + "]";
        } else {
            try {
                JSONArray jsonEstablecimientos = new JSONArray(establecimientos);
                jsonEstablecimientos.put(e.toJSON());
                establecimientosString = jsonEstablecimientos.toString();
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
        putShared(activity, FAVORITOS, establecimientosString);
    }
    public static void removeEstablecimientoFavorito(Context activity, String codigo) {
        int targetIndex = 0;
        for (Establecimiento e : establecimientos_favoritos) {
            if (codigo.equals(e.getCodigo())) {
                break;
            }
            targetIndex++;
        }

        establecimientos_favoritos.remove(targetIndex);

        /*COMIT CHANGES TO SHARED PREFERENCES*/
        JSONArray jsonEstablecimientos = new JSONArray();
        for (Establecimiento e : establecimientos_favoritos) {
            jsonEstablecimientos.put(e.toJSON());
        }
        String establecimientosString = jsonEstablecimientos.toString();

        putShared(activity, FAVORITOS, establecimientosString);
    }
    /*END FAVORITOS*/

    /*REPORTE*/
    private static Stack<Fragment> currentFragment;
    public static void pushFragment(Fragment fragment) {
        currentFragment.push(fragment);
    }
    public static Fragment getCurrentFragment()
    {
        return currentFragment.get(currentFragment.size()-1);
    }

    /*END REPORTE*/



    public static void showAlertDialog(final Activity activity, String title, String mensaje, int iconId, String textoBoton)
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();

        View v = inflater.inflate(R.layout.dialog_accept, null);


        Button btn = v.findViewById(R.id.bt_close);
        //TITULO
        TextView titleView = v.findViewById(R.id.title);
        titleView.setText(title);
        //CONTENT
        TextView content = v.findViewById(R.id.content);
        content.setText(mensaje);
        //ICONO
        ImageView icon = v.findViewById(R.id.icon);
        icon.setImageResource(iconId);
        //TEXTO DEL BOTON
        if(textoBoton!=null){
            btn.setText(textoBoton);
        }
        v.findViewById(R.id.header).setBackgroundResource(R.color.red_300);
        v.findViewById(R.id.bt_close).setBackgroundResource(R.drawable.btn_rounded_red);
        builder.setView(v);
        final AlertDialog alerta = builder.create();
        alerta.show();
        btn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        alerta.dismiss();
                    }
                }

        );
    }



    public static void showMessageDialog(Activity activity,String title, String mensaje, int iconId, String textoBoton)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();

        View v = inflater.inflate(R.layout.dialog_accept, null);


        Button btn = v.findViewById(R.id.bt_close);
        //TITULO
        TextView titleView = v.findViewById(R.id.title);
        titleView.setText(title);
        //CONTENT
        TextView content = v.findViewById(R.id.content);
        content.setText(mensaje);
        //ICONO
        ImageView icon = v.findViewById(R.id.icon);
        icon.setImageResource(iconId);
        //TEXTO DEL BOTON
        if(textoBoton!=null){
            btn.setText(textoBoton);
        }

        builder.setView(v);
        final AlertDialog alerta = builder.create();
        alerta.show();
        btn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        alerta.dismiss();
                    }
                }

        );
    }

    public static void showProgresBar(String texto, Context context )
    {
        progressDialog= new ProgressDialog(context);
        progressDialog.setMessage(texto);
        try
        {
            progressDialog.show();
        }
        catch (Exception e)
        {

        }

    }
    public static  void hideProgressBar()
    {
        try
        {
            progressDialog.dismiss();
        }
        catch  (Exception e)
        {

        }

    }

    /*SHARED PREFERENCES*/
    public static final String FAVORITOS = "favoritos";
    public static final String ENVIADOS = "enviados";
    public static final String SIN_ENVIAR = "encolados";

    public static String getShared(Context activity, String key, String defValue)
    {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity);
        return sp.getString(key, defValue);
    }
    public static void putShared(Context activity, String key, String value)
    {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /* END SHARED PREFERENCES*/

        public static  boolean validateInternetConnection(Context activity)
        {
            ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (!(networkInfo != null && networkInfo.isConnected()))
            {
                return false;
            }
            return true;
         /*  try
            {
                Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");

                int val           = p.waitFor();
                boolean reachable = (val == 0);
                return reachable;

            } catch (Exception e)
            {
                e.printStackTrace();
            }
            return false;*/
        }


    public static String getMD5(String input)
    {
        try
        {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);

            while (hashtext.length() < 32)
            {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new RuntimeException(e);
        }
    }
    @SuppressLint("WrongConstant")
    public static boolean checkWriteExternalStorage(Context context)
    {
        if (checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            {
                ActivityCompat.requestPermissions(mainActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},0);
                return false;
            }
        else
            {
                return true;
            }
    }

    public static   boolean Vacio(EditText campo)
    {
        String dato = campo.getText().toString().trim();
        if(TextUtils.isEmpty(dato)){
            campo.setError("Campo Requerido");
            campo.requestFocus();
            return true;
        }
        else{
            return false;
        }
    }
}
