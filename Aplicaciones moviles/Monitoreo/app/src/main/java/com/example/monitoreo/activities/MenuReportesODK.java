package com.example.monitoreo.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.monitoreo.R;
import com.example.monitoreo.adapters.AdapterFormulario;
import com.example.monitoreo.modelos.Formulario;
import com.example.monitoreo.utils.OpenBlankForm;
import com.example.monitoreo.utils.commons;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.Normalizer;
import java.util.ArrayList;

import static com.example.monitoreo.utils.commons.getShared;
import static com.example.monitoreo.utils.commons.hideProgressBar;
import static com.example.monitoreo.utils.commons.mainActivity;
import static com.example.monitoreo.utils.commons.putShared;
import static com.example.monitoreo.utils.commons.queue;
import static com.example.monitoreo.utils.commons.setQueue;
import static com.example.monitoreo.utils.commons.showProgresBar;
import static com.example.monitoreo.utils.commons.validateInternetConnection;

public class MenuReportesODK extends AppCompatActivity
{
    static RecyclerView recList;
    static AdapterFormulario adapterFormulario;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_reportes_o_d_k);

        commons.lastStep=MenuReportesODK.this;
        recList = this.findViewById(R.id.listFormularios);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        registerForContextMenu(recList);

        setFormularios(getApplicationContext());
    }


    AdapterFormulario.OnSelectClick onSelectClick = new AdapterFormulario.OnSelectClick()
    {
        @Override
        public void onSelectClick(View view, Formulario obj, int position)
        {
            OpenBlankForm.openBlankForm(obj, MenuReportesODK.this);
        }
    };

    private  void setFormularios(Context context)
    {
        final Gson gson = new Gson();
        boolean flag = validateInternetConnection(context);
        if ( !flag)
        {
            getFormulariosPC(context);
        }
        else
        {
            showProgresBar("Obteniendo información del servidor ...", mainActivity);
            ArrayList<Formulario> formularioList = new ArrayList<>();

            // Instantiate the RequestQueue.
            setQueue(context);

            String url = commons.RDDAPI_URL + "getFormularios";
            JSONArray jsonBody = new JSONArray();
            JsonArrayRequest request = new JsonArrayRequest
                    (Request.Method.GET, url, jsonBody, new Response.Listener<JSONArray>()
                    {
                        @Override
                        public void onResponse(JSONArray response)
                        {
                            String JSONString = response.toString();
                            try
                            {
                                    ArrayList<Formulario> formularios = gson.fromJson(response.toString(), new TypeToken<ArrayList<Formulario>>(){}.getType());
                                    adapterFormulario = new AdapterFormulario(formularios);
                                    recList.setAdapter(adapterFormulario);
                                    adapterFormulario.setOnSelectClick(onSelectClick);
                                    putShared(getApplicationContext(),"ArrayFormsODK",response.toString());

                            } catch (Throwable tx)
                            {
                                System.out.println("error : "+ tx.toString());
                            }
                            hideProgressBar();
                        }
                    }, new Response.ErrorListener()
                    {

                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            getFormulariosPC(mainActivity);
                            hideProgressBar();
                        }
                    });
            queue.add(request);
        }
    }

    public void getFormulariosPC(Context context)
    {
        String formularios = getShared(context, "ArrayFormsODK","");
        if (formularios.equals(""))
        {
            commons.showAlertDialog(mainActivity, "Sin conexión!",
                    "El dispositivo no cuenta con una conexión a internet en este momento. Esta acción requiere de internet.",
                    R.drawable.ic_signal_wifi_off, null);
            finish();
        }
        else
        {
            Gson gson = new Gson();
            ArrayList<Formulario> form = gson.fromJson(formularios, new TypeToken<ArrayList<Formulario>>(){}.getType());
            adapterFormulario = new AdapterFormulario(form);
            recList.setAdapter(adapterFormulario);
            adapterFormulario.setOnSelectClick(onSelectClick);
        }
    }
}