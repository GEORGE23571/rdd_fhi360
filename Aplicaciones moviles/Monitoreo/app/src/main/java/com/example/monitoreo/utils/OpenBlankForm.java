package com.example.monitoreo.utils;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.BaseColumns;
import com.example.monitoreo.R;
import com.example.monitoreo.activities.busquedaCentro;
import com.example.monitoreo.modelos.Establecimiento;
import com.example.monitoreo.modelos.Formulario;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;



public class OpenBlankForm
{

    public static final String FORMS_URI = "content://org.odk.collect.android.provider.odk.forms/forms/";
    public static final String INSTANCES_URI = "content://org.odk.collect.android.provider.odk.instances/instances/";
    public static String PATHODK="";


    public static void openBlankForm(Formulario form, Activity activity)
    {
        System.out.println("muñoz *****"+form.getBody_xml()+"***");
        Map<String, Integer> forms = getFormList(getFormsCursor(activity));
        String expectedJrFormId = form.getId_form();

        if (forms.containsKey(expectedJrFormId))
        {

           /* int id = forms.get(expectedJrFormId);
            Intent i = new Intent(Intent.ACTION_EDIT, Uri.parse(FORMS_URI + "/" + id));
            activity.startActivity(i);*/

                Intent i = new Intent(activity, busquedaCentro.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("JrFormId", expectedJrFormId);
                activity.startActivity(i);

        }
        else
        {
            commons.showAlertDialog(activity,
                    "No se encontro el formulario!", "Se requiere descargar el formulario de ODK correpondiente",
                    R.drawable.ic_receipt, null);
        }
    }

    private static Cursor getFormsCursor( Activity activity)
    {
        return activity.getContentResolver().query(Uri.parse(FORMS_URI), null, null, null, null);
    }

    private static Map<String, Integer> getFormList(Cursor cursor)
    {

        Map<String, Integer> forms = new HashMap<>();
        if (cursor != null)
        {
            try {
                    while (cursor.moveToNext())
                    {
                        int id = cursor.getInt(cursor.getColumnIndex(BaseColumns._ID));
                        String jrFormId = cursor.getString(cursor.getColumnIndex("jrFormId"));
                        forms.put(jrFormId, id);
                    }
            } finally
            {
                cursor.close();
            }
        }
        return forms;
    }


    public static void newFormOdk(Activity activity, Establecimiento centro, String jrIdForm, String version, String  xmlStructure )
    {
        int newIdInstance =nextIdODK(activity);

        //String path = String.valueOf(Environment.getExternalStorageDirectory()) + "/Android/data/org.odk.collect.android/files/instances/"+newIdInstance+"/";
        rutaODK(Environment.getExternalStorageDirectory().getAbsolutePath());
        String path = String.valueOf(PATHODK+"/files/instances/"+newIdInstance+"/");

        File carpeta = new File(path+newIdInstance+".xml");
        File dsfk = new File(path);

        dsfk.mkdir();

        if (!carpeta.exists())
        {
            try
            {
                carpeta.createNewFile();

            } catch (IOException e)
            {
                System.out.println("***** error de escritura" + e.getMessage());
                System.out.println("***** "+path);
            }
        }
        else
        {

        }

        try
        {
            FileWriter writer = new FileWriter(carpeta);
            String cuerpo = xmlStructure.replaceAll("<centro></centro>","<centro>"+centro.getCodigo()+"</centro>");
            writer.append(cuerpo);
            writer.flush();
            writer.close();
            System.out.println("cuerpo  "+centro.getCodigo()+"  "+cuerpo);
        }
        catch (Exception e)
        {
            System.out.println("error en datos : "+centro.getCodigo()+"  "+e.getMessage());
            e.printStackTrace();
        }

        insertarFormODK(activity,newIdInstance, centro.getCodigo(), jrIdForm,version);

        Intent i = new Intent(Intent.ACTION_EDIT, Uri.parse(INSTANCES_URI + newIdInstance));
        activity.startActivity(i);
    }

    private static int  nextIdODK(Activity activity)
    {
        final String[] projection = new String[]{"_id"};
        Cursor cur;
        int max =-1;
        try
        {
            cur =activity.getContentResolver().query(Uri.parse(INSTANCES_URI), projection,"" , null, null);

            if (cur.moveToFirst())
            {
                do
                {
                    //list.add(new Sms(cur.getString(0),cur.getString(1),cur.getString(2)));
                    int valor = Integer.parseInt(cur.getString(0));
                    if (valor>max)
                    {
                        max=valor;
                    }
                }
                while (cur.moveToNext());
            }
            max++;
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return max;
    }

    private static void  insertarFormODK(Activity activity,int _id, String cod_centro, String jrIdForm,String version)
    {
        /*
    _id                 INTEGER PRIMARY KEY,
    displayName         TEXT    NOT NULL,
    submissionUri       TEXT,
    canEditWhenComplete TEXT,
    instanceFilePath    TEXT    NOT NULL,
    jrFormId            TEXT    NOT NULL,
    jrVersion           TEXT,
    status              TEXT    NOT NULL,
    date                DATE    NOT NULL,
        * */
        ContentValues values = new ContentValues();
        values.put("_id",_id);
        values.put("displayName",cod_centro);
        values.put("canEditWhenComplete", "true");
        values.put("instanceFilePath",_id+"/"+_id+".xml");
        values.put("jrFormId", jrIdForm);
        values.put("jrVersion", version);
        values.put("status", "incomplete");
        activity.getContentResolver().insert(Uri.parse(INSTANCES_URI),values);
    }


    public static void rutaODK(String ruta)
    {
        File file = new File(ruta);
        if(file.getName().equals("org.odk.collect.android"))
        {
            PATHODK= file.getPath();
            System.out.println("ruta encontrada "+ PATHODK);
        }
        else {
            File[] archivos = file.listFiles();
            for (File f : archivos) {
                if (f.isDirectory()) {
                    rutaODK(f.getPath());
                }
            }
        }
    }
}