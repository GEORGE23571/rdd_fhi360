package com.example.monitoreo.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.monitoreo.R;
import com.example.monitoreo.utils.commons;

import org.json.JSONObject;

import static com.example.monitoreo.utils.commons.Vacio;
import static com.example.monitoreo.utils.commons.getMD5;
import static com.example.monitoreo.utils.commons.hideProgressBar;
import static com.example.monitoreo.utils.commons.mainActivity;
import static com.example.monitoreo.utils.commons.queue;
import static com.example.monitoreo.utils.commons.putShared;
import static com.example.monitoreo.utils.commons.setQueue;
import static com.example.monitoreo.utils.commons.showProgresBar;

public class login extends AppCompatActivity
{
    private EditText txtUser;
    private EditText txtPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mainActivity.finish();
        txtUser= findViewById(R.id.txtUser);
        txtPassword=findViewById(R.id.txtPass);

        (findViewById(R.id.btnEnter)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                Login();
            }
        });
    }

    private boolean Login()
    {

        if(Vacio(txtUser) && Vacio(txtPassword))
        {

        }
        else
        {
            showProgresBar("Obteniendo información del servidor ...", this);
            // Instantiate the RequestQueue.
            setQueue(mainActivity);
            String url = commons.RDDAPI_URL + "Login";
            JSONObject jsonBody = new JSONObject();
            try
            {
                jsonBody.put("user", txtUser.getText().toString());
                jsonBody.put("password",getMD5( txtPassword.getText().toString()));
            }
            catch(Exception e )
            {

            }
            JsonObjectRequest request = new JsonObjectRequest
                    (Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>()
                    {
                        @Override
                        public void onResponse(JSONObject response)
                        {
                            hideProgressBar();
                            String JSONString = response.toString();
                            try
                            {
                                JSONObject res = new JSONObject(JSONString);
                                //{"user":"george","nom_user":"George Carrillo","password":"9290777a9429b2b193f8169e3d54699c","rol":"administrador","status":true}
                                if (Boolean.parseBoolean(  res.getString("status")))
                                {
                                    putShared(getApplication(),"user",res.getString("user"));
                                    putShared(getApplication(),"nom_user",res.getString("nom_user"));

                                    commons.startActivity(getApplicationContext(), MainActivity.class);
                                    login.this.finish();
                                }
                                else
                                {
                                    commons.showAlertDialog(login.this, "Usuario no encontrado!",
                                            res.getString("message") ,
                                            R.drawable.ic_account_circle, null);
                                }
                            } catch (Throwable tx)
                            {
                                System.out.println("*****error " + tx.getMessage());
                            }

                        }
                    }, new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            hideProgressBar();
                            commons.showAlertDialog(login.this, "Problemas con el servidor!.",
                                    "Intente a más tarde.",
                                    R.drawable.ic_cloud_off, null);
                        }
                    });
            queue.add(request);
        }
        return false;
    }
}