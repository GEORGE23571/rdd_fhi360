package com.example.monitoreo.modelos;

public class Nivel
{
    private int id_muni;
    private int id_depto;
    private int id_nivel;
    private String nivel;

    public int getId_nivel() {
        return id_nivel;
    }

    public String getNivel() {
        return nivel;
    }

    public void setId_nivel(int id_nivel) {
        this.id_nivel = id_nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public int getId_muni() {
        return id_muni;
    }

    public int getId_depto() {
        return id_depto;
    }

    public void setId_muni(int id_muni) {
        this.id_muni = id_muni;
    }

    public void setId_depto(int id_depto) {
        this.id_depto = id_depto;
    }
}
