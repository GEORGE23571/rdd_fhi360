package com.example.monitoreo.adapters;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.monitoreo.R;
import com.example.monitoreo.modelos.Formulario;

import java.util.List;

public class AdapterFormulario extends RecyclerView.Adapter<AdapterFormulario.FormularioViewHolder>
{

    private List<Formulario> formularioList;

    private OnSelectClick mOnSelectClick;

    public interface OnSelectClick
    {
        void onSelectClick(View view,Formulario obj, int position);
    }

    public void setOnSelectClick(final OnSelectClick mOnDeleteClick)
    {
        this.mOnSelectClick = mOnDeleteClick;
    }

    public AdapterFormulario(List<Formulario> formularioList)
    {
        this.formularioList= formularioList ;
    }

    @NonNull
    @Override
    public FormularioViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType)
    {
        final View itemView = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.cardview_formulario,viewGroup,false);

        return new FormularioViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FormularioViewHolder holder, int i)
    {
        Formulario form=formularioList.get(i);
        holder.txtFormulario.setText(form.getNombre());
    }

    @Override
    public int getItemCount()
    {
        return formularioList.size();
    }


    public  class FormularioViewHolder extends RecyclerView.ViewHolder
    {
        protected TextView txtFormulario;

        public FormularioViewHolder(final View itemView)
        {
            super(itemView);
            itemView.setClickable(true);
            txtFormulario = itemView.findViewById(R.id.txtFormulario);
            txtFormulario.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (mOnSelectClick == null) return;
                    mOnSelectClick.onSelectClick(itemView, formularioList.get(getAdapterPosition()),getAdapterPosition());
                }
            });
        }
    }
}
