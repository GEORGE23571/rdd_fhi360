package com.example.monitoreo.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.monitoreo.R;
import com.example.monitoreo.utils.commons;

import static com.example.monitoreo.utils.commons.checkWriteExternalStorage;
import static com.example.monitoreo.utils.commons.getShared;
import static com.example.monitoreo.utils.commons.initBD;
import static com.example.monitoreo.utils.commons. mainActivity;
import static com.example.monitoreo.utils.commons.putShared;

public class MainActivity extends AppCompatActivity
{
    private TextView txtNombreUsuario;
    private TextView txtUsuario;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainActivity=MainActivity.this;
        initBD(mainActivity);
        setButtons();
        validarUser();
        if (checkWriteExternalStorage(MainActivity.this))
        {
            //  this.finish();
        }
        else
        {

        }
    }
    @Override
    public void onResume()
    {
        super.onResume();
        //debugShared();
    }

    public void validarUser()
    {
        String user = getShared(getApplicationContext(), "nom_user","");

        if (!commons.estaInstaladaAplicacion("org.odk.collect.android", getApplicationContext()))
        {
            commons.showAlertDialog(MainActivity.this ,
                    "Sin ODK!", "Se requiere tener instalada la aplicación ODK Collect. \n Por favor diríjase a la Play Store para su descarga..",
                    R.drawable.ic_search, null);
        }



    }


    private void setButtons()
    {
        /*REPORTAR*/
        (findViewById(R.id.btn_reportar)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //   commons.startActivity(getApplicationContext(), busquedaCentro.class);
                commons.startActivity(getApplicationContext(), MenuReportesODK.class);
            }
        });

        (findViewById(R.id.btn_salir)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finish();
               /* commons.startActivity(getApplicationContext(), login.class);
                putShared(getApplication(),"user","");
                putShared(getApplication(),"nom_user","");*/
            }
        });


        (findViewById(R.id.btn_configuracion)).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view)
                    {
                        Intent intent;
                        try
                        {
                            PackageManager manager = getPackageManager();
                            intent  = manager.getLaunchIntentForPackage("org.odk.collect.android");
                            intent .addCategory(Intent.CATEGORY_LAUNCHER);
                            startActivity(intent);
                        }
                        catch(Exception e)
                        {
                            commons.showAlertDialog(MainActivity.this ,
                                    "Sin ODK!", "Se requiere tener instalada la aplicación ODK Collect. \n Por favor diríjase a la Play Store para su descarga..",
                                    R.drawable.ic_search, null);
                        }


                    }
                });
    }
}