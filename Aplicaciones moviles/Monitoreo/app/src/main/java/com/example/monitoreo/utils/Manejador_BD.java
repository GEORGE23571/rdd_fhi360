package com.example.monitoreo.utils;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Criteria;

import com.example.monitoreo.modelos.Departamento;
import com.example.monitoreo.modelos.Establecimiento;
import com.example.monitoreo.modelos.Jornada;
import com.example.monitoreo.modelos.Nivel;
import com.example.monitoreo.modelos.municipio;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class Manejador_BD extends SQLiteOpenHelper
{
    private static final String DB_NAME = "DB_RDD_SUPERVISOR.db";
    private static  String DB_PATH="";
    private static SQLiteDatabase mDataBase;
    private static final int DB_VERSION = 3;
    private Context mContext=null;

    public Manejador_BD(Context context)
    {
        super(context, DB_NAME, null, DB_VERSION);
        rutaDB(context.getApplicationInfo().dataDir);
        mContext=context;
    }
    public static void rutaDB(String ruta)
    {
        File file = new File(ruta);
        if(file.getName().equals("com.example.monitoreo"))
        {
            DB_PATH= file.getPath() +"/databases/";
        }
        else {
            File[] archivos = file.listFiles();
            for (File f : archivos) {
                if (f.isDirectory())
                {
                    rutaDB(f.getPath());
                }
            }
        }
    }
    @Override
    public synchronized  void close()
    {
        if(mDataBase!=null)
            mDataBase.close();
        super.close();
    }
    public void openDataBase() throws SQLException
    {
        // Open the database
        String myPath = DB_PATH + DB_NAME;
        System.out.println("ruta "+ myPath);
        mDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    public void CrearDB()
    {
        boolean dbExist= checkDB();

        if(dbExist)
        {
            //la base de datos ya existe
        }
        else
        {
            this.getReadableDatabase();
            try
            {
                copyDB();
            }
            catch(Exception e)
            {
                throw new Error("Error copiando DB");
            }
        }
    }

    public boolean checkDB()
    {
        SQLiteDatabase tempdb=null;
        try
        {
            String path =DB_PATH+DB_NAME;
            tempdb= SQLiteDatabase.openDatabase(path,null,SQLiteDatabase.OPEN_READWRITE);
        }
        catch(Exception e){}

        if(tempdb!=null)
            tempdb.close();
        return tempdb!=null?true:false;
    }

    private void copyDB  ()
    {
        try
        {
            InputStream myInput = mContext.getAssets().open(DB_NAME);
            String outFileName=DB_PATH+DB_NAME;
            OutputStream myOutput=new FileOutputStream(outFileName);
            byte[] buffer =new byte[1024];
            int length;
            while((length = myInput.read(buffer))>0)
            {
                myOutput.write(buffer,0,length);
            }
            myOutput.flush();
            myOutput.close();
            myInput.close();
        }
        catch (Exception e) { }

    }

    public ArrayList<Departamento> getDepartamento()
    {
        String query ="select * from RDD_DEPARTAMENTO\n " +
                         " WHERE COD_ESTADO=1";

        ArrayList<Departamento> arreglo = new ArrayList<>();
        Cursor cursor= select (query );
        cursor.moveToFirst();

        if (cursor.moveToFirst())
        {
            do
            {
                Departamento element = new Departamento();
                element.setId_depto(cursor.getString(cursor.getColumnIndex("ID_DEPARTAMENTO")));
                element.setNom_depto(cursor.getString(cursor.getColumnIndex("NOMBRE")));
                ;
                arreglo.add(element);
            } while(cursor.moveToNext());
        }

        cursor.close();
        return arreglo;
    }
    public ArrayList<municipio> getMunicipio(String cod_depto)
    {
        String query ="select * from RDD_MUNICIPIO \n" +
                "     WHERE ID_DEPARTAMENTO='"+cod_depto+"' AND COD_ESTADO=1";

        ArrayList<municipio> arreglo = new ArrayList<>();
        Cursor cursor= select (query );

        cursor.moveToFirst();

        if (cursor.moveToFirst())
        {
            do
            {
                municipio element = new municipio(cursor.getString(cursor.getColumnIndex("ID_DEPARTAMENTO")),
                        cursor.getString(cursor.getColumnIndex("ID_MUNICIPIO")),
                        cursor.getString(cursor.getColumnIndex("NOMBRE")));

                arreglo.add(element);
            } while(cursor.moveToNext());
        }

        cursor.close();
        return arreglo;
    }
    public ArrayList<Nivel> getNivel(String depto)
    {
        String query ="SELECT DISTINCT A.ID_NIVEL , A.NOMBRE FROM RDD_NIVEL  A " +
                "INNER JOIN RDD_ESTABLECIMIENTO  B ON A.ID_NIVEL =B.ID_NIVEL " +
                "WHERE B.ID_DEPARTAMENTO ='"+depto +"' ";

        ArrayList<Nivel> arreglo = new ArrayList<>();
        Cursor cursor= select (query );

        cursor.moveToFirst();

        if (cursor.moveToFirst())
        {
            do
            {
                Nivel element = new Nivel();
                element.setNivel(cursor.getString(cursor.getColumnIndex("NOMBRE")));
                element.setId_nivel( Integer.parseInt( cursor.getString(cursor.getColumnIndex("ID_NIVEL"))));

                arreglo.add(element);
            }
            while(cursor.moveToNext());
        }

        cursor.close();
        return arreglo;
    }
    public ArrayList<Jornada> getJornada(String depto)
    {
        String query =" SELECT DISTINCT A.ID_JORNADA , A.NOMBRE FROM RDD_JORNADA  A " +
                "INNER JOIN RDD_ESTABLECIMIENTO  B ON A.ID_JORNADA =B.ID_JORNADA " +
                "WHERE B.ID_DEPARTAMENTO ='"+depto  +"';";
        ArrayList<Jornada> arreglo = new ArrayList<>();
        Cursor cursor= select (query );

        cursor.moveToFirst();

        if (cursor.moveToFirst())
        {
            do
            {
                Jornada element = new Jornada();
                element.setNombre(cursor.getString(cursor.getColumnIndex("NOMBRE")));
                element.setId( cursor.getString(cursor.getColumnIndex("ID_JORNADA")));

                arreglo.add(element);
            } while(cursor.moveToNext());
        }

        cursor.close();
        return arreglo;
    }

    public ArrayList<Establecimiento> getEstablecimiento(String depto, String mun, String nivel, String jornada)
    {

        String query ="SELECT * FROM RDD_ESTABLECIMIENTO " ;

        ArrayList<String> criterios=new ArrayList<>();

        if(depto!=null)
          criterios.add("ID_DEPARTAMENTO ='"+depto+"' ");
        if(mun!=null)
            criterios.add("ID_MUNICIPIO ='"+mun+"' ");
        if(nivel!=null)
            criterios.add("ID_NIVEL ='"+nivel+"' ");
        if(jornada!=null)
            criterios.add("ID_JORNADA ='"+jornada+"' ");

        //"WHERE ID_DEPARTAMENTO ='"+depto+"' AND ID_MUNICIPIO='"+mun+"' AND ID_NIVEL="+nivel+" AND ID_JORNADA="+jornada;

        if (criterios.size()>0)
        {
            query += " WHERE ";
            query+=" "+criterios.get(0);
            for (int i =1;i<criterios.size();i++)
            {
                query +=" AND "+ criterios.get(i);
            }
        }
        ArrayList<Establecimiento> arreglo = new ArrayList<>();
        Cursor cursor= select (query );

        cursor.moveToFirst();

        if (cursor.moveToFirst())
        {
            do
            {
                Establecimiento element = new Establecimiento();
                element.setCodigo(cursor.getString(cursor.getColumnIndex("ID_DEPARTAMENTO"))+"-"+
                        cursor.getString(cursor.getColumnIndex("ID_MUNICIPIO"))+"-"+
                                cursor.getString(cursor.getColumnIndex("ID_ESTAB"))+"-"+
                                cursor.getString(cursor.getColumnIndex("ID_NIVEL"))
                        );
                element.setNombre(cursor.getString(cursor.getColumnIndex("NOMBRE")));
                element.setDireccion( cursor.getString(cursor.getColumnIndex("DIRECCION")));
                element.setJornada(cursor.getString(cursor.getColumnIndex("ID_JORNADA")));
                arreglo.add(element);
            } while(cursor.moveToNext());
        }

        cursor.close();
        return arreglo;
    }

    public Cursor select(String query) throws SQLException
    {
        return mDataBase.rawQuery(query,null);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {}

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {}
}