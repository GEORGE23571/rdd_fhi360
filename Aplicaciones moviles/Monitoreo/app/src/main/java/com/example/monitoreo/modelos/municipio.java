package com.example.monitoreo.modelos;

public class municipio
{
    public String depto;
    public String muni;
    public String municipio;


    public municipio(String depto, String muni_, String municipio)
    {
        this.depto = depto;
        this.municipio = municipio;
        this.muni=muni_;
    }

    public String getDepto() {
        return depto;
    }

    public String getMunicipio() {
        return municipio;
    }

    public String getMuni() {
        return muni;
    }

    public void setDepto(String depto) {
        this.depto = depto;
    }

    public void setMuni(String muni) {
        this.muni = muni;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }
}
