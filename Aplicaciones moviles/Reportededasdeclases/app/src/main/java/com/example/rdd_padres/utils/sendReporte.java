package com.example.rdd_padres.utils;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.rdd_padres.modelos.Reporte;
import com.example.rdd_padres.modelos.Respuesta;
import com.example.rdd_padres.modelos.SmsToSent;
import com.google.gson.Gson;
import com.example.rdd_padres.commons;
import com.example.rdd_padres.R;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.util.ArrayList;

import static com.example.rdd_padres.catalogos.COD_ENVIO_SMS;
import static com.example.rdd_padres.catalogos.COD_ESTADO_ENVIADO;
import static com.example.rdd_padres.catalogos.COD_ESTADO_SIN_ENVIAR;
import static com.example.rdd_padres.catalogos.MaxLengthSMS;
import static com.example.rdd_padres.commons.BD;
import static com.example.rdd_padres.commons.getShared;
import static com.example.rdd_padres.commons.mainActivity;
import static com.example.rdd_padres.commons.queue;
import static com.example.rdd_padres.commons.setQueue;
import static com.example.rdd_padres.commons.validateInternetConnection;


public class sendReporte
{
    private static ArrayList<SmsToSent> listMensajes;
    private static int envios ;

    public static void enviar(Context activity, Boolean mostrarMensaje)
    {
        BD.setReporte(commons.reporte);
        commons.reporte = null;
    }

    public static void enviar_internet(Context activity, Boolean  mostrarMensaje )
    {
        if (validateInternetConnection(activity))
        {
            rdd_setReporte(activity );
        }
    }

   public static boolean enviar_sms(final Context activity, Boolean  mostrarMensaje)
   {

       listMensajes = getReportesSms();
       int mensajes  =listMensajes.size() ;
       envios=0;

       enviar_sms(activity);
       if (mostrarMensaje)
       {
           if (mensajes>0)
           {

               commons.showMessageDialog(mainActivity, "Reportes enviados!",
                       "Se han enviado los reportes por mensaje de texto, puede verificar el estado en reportes llenados",
                       R.drawable.ic_enviados, null);
            return  true;
           }
           else
           {
               commons.showMessageDialog(mainActivity, "Reporte almacenado!",
           "Su reporte será envíado cuando se alcance la cantidad máxima de reportes" +
                   " para ser enviados por mensaje de texto. Puede visualizar los reportes en espera " +
                   "de ser enviados en el botón \"Ver Reportes Llenados\" del menú principal",
                       R.drawable.ic_save, null);
           }
       }
       return (mensajes>0);
   }

   private static void enviar_sms(final Context activity)
   {
       try
       {
           if (listMensajes.isEmpty() )
           {
               return;
           }
           SmsManager smsManager = SmsManager.getDefault();
           PendingIntent sentPI, deliveredPI;
           sentPI = PendingIntent.getBroadcast(activity, 0, new Intent("SMS SENT"), 0);
           deliveredPI = PendingIntent.getBroadcast(activity, 0, new Intent("SMS DELIVERED"), 0);

           BroadcastReceiver confirmSentBR = new BroadcastReceiver( )
           {
               @Override
               public void onReceive(Context context, Intent intent)
               {
                   int COD_ESTADO;
                   switch (getResultCode())
                   {
                       case Activity.RESULT_OK:
                           //Toast.makeText(activity, "SMS sent successfully", Toast.LENGTH_SHORT).show();
                           COD_ESTADO=COD_ESTADO_ENVIADO;
                           envios++;

                           break;

                       default: //all other codes are error
                           //Toast.makeText(activity,"Error: SMS was not sent", Toast.LENGTH_SHORT).show();
                           COD_ESTADO=COD_ESTADO_SIN_ENVIAR;
                           break;
                   }
                   System.out.println("envios "+envios+" "+COD_ESTADO);
                  if (!listMensajes.isEmpty())
                  {
                      for (int id: listMensajes.get(0).getId_Reportes())
                      {
                          BD.setEstadoReporte( id,COD_ESTADO);
                      }
                      listMensajes.remove(0);
                      if (listMensajes.size()>0)
                      {
                          enviar_sms(activity);
                      }
                  }

               }
           };

           BroadcastReceiver confirmDeliveryBR = new BroadcastReceiver( )
           {
               @Override
               public void onReceive(Context context, Intent intent) {
                   switch (getResultCode()) {
                       case Activity.RESULT_OK:
                           break;
                       default: //all other codes are error
                           break;
                   }
               }
           };

           IntentFilter confirmSentIntentFilter = new IntentFilter("SMS SENT");
           IntentFilter confirmDeliveryIntentFilter = new IntentFilter("SMS DELIVERED");
           activity.registerReceiver(confirmSentBR, confirmSentIntentFilter);

           String tel =getShared(mainActivity, "TEL","");
           if(tel.equals(""))
               {

               }
           else
               {
                   System.out.println("Se enviará el mensjae al celular "+ tel);
                   smsManager.sendTextMessage( tel, null, listMensajes.get(0).getBody(), sentPI, deliveredPI );
               }

       }
       catch(Exception e)
       {

           System.out.println("***** error al enviar mensaje por sms "+e.getMessage());
       }
   }

   public  static ArrayList<SmsToSent> getReportesSms()
   {
       ArrayList<SmsToSent> mensajes = new ArrayList<>();
       ArrayList<Reporte> listReportes =
               BD.getReportes("select * from REPORTE where ESTADO = "+COD_ESTADO_SIN_ENVIAR+
                                                           " and TIPO_ENVIO = "+COD_ENVIO_SMS+" ;");
       boolean flag =false;
       while (listReportes.size()>0)
       {
           String jsonAnt ="";

           int i =0;
           for (; i< listReportes.size();i++)
           {
               String cadena ="";
               ArrayList<Integer> ids =new ArrayList<>();

               for (int j=0;j<i;j++)
               {
                   cadena=cadena+listReportes.get(j).getJsonFromReporteSMS().toString()+";";
                   ids.add(listReportes.get(j).getId());
               }

               cadena=cadena+listReportes.get(i).getJsonFromReporteSMS().toString();
               ids.add(listReportes.get(i).getId());

               if (cadena.length()> MaxLengthSMS)
               {
                  jsonAnt= jsonAnt.replace("{","(");
                  jsonAnt= jsonAnt.replace("}",")");
                   mensajes.add(new SmsToSent(jsonAnt,ids));
                   flag=true;
                   break;
               }
               else
               {
                   flag=false;
               }
               jsonAnt=cadena;
           }

           for (int j=0;j<i ;j++)
           {
               listReportes.remove(0);
           }
       }
       if (mensajes.size()==1 && flag)
       {
           mensajes.remove(0);
       }

        return mensajes;
   }

  private static void rdd_setReporte(final Context activity)
   {
       setQueue(activity);
       String url = commons.RDDAPI_URL + "setReporte";
       JSONArray jsonBody  = new JSONArray();

       ArrayList<Reporte> listReportes = BD.getReportes("select * from REPORTE where ESTADO = "+COD_ESTADO_SIN_ENVIAR+" ;");

       if (listReportes.size()==0)
       {
           return;
       }

       for(Reporte element: listReportes)
       {
           BD.setEstadoReporte(element.getUri());
           jsonBody.put(element.getJsonFromReporte());
       }

       System.out.println(jsonBody.toString());

       JsonArrayRequest request = new JsonArrayRequest

       (Request.Method.POST, url,jsonBody, new Response.Listener<JSONArray>()
       {
           @Override
           public void onResponse(JSONArray response)
           {
               responseFunction( response);
           }
       }, new Response.ErrorListener()
       {
           @Override
           public void onErrorResponse(VolleyError error)
           {
               error.printStackTrace();
           }
       });
      /* request.setRetryPolicy(new DefaultRetryPolicy(
               DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
               DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
               DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
*/
       request.setRetryPolicy(new RetryPolicy()
       {
           @Override
           public int getCurrentTimeout() {
               return 50000;
           }

           @Override
           public int getCurrentRetryCount() {
               return 50000;
           }

           @Override
           public void retry(VolleyError error) throws VolleyError
           {

           }
       });
       queue.add(request);
   }

   private static void responseFunction(JSONArray response)
    {
        try
        {
            Gson gson = new Gson();
            ArrayList<Respuesta> respuestas = gson.fromJson(response.toString(),  new TypeToken<ArrayList<Respuesta>>(){}.getType());
            BD.setEstadoReporte(respuestas);
        }
        catch (Throwable tx)
        {
            tx.printStackTrace();
        }
    }

}
