package com.example.rdd_padres.modelos;

public class Materia
{
    private String nombre ;
    private Boolean estado =false;

    public Materia(String nombre_)
    {
        this.nombre=nombre_;
    }

    public String getNombre() {
        return nombre;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
    public void changeEstado()
    {
        estado=!estado;
    }

}
