package com.example.rdd_padres.utils;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.widget.Toast;
import static android.provider.Telephony.TextBasedSmsColumns.MESSAGE_TYPE_SENT;
import static com.example.rdd_padres.catalogos.LAST_DATE_READ_INBOX;
import static com.example.rdd_padres.commons.getShared;
import static com.example.rdd_padres.commons.mainActivity;

public class SmsOutgoingObserver extends ContentObserver
{
    /**
     * Creates a content observer.
     *
     * @param handler The handler to run {@link #onChange} on, or null if none.
     */
    public int v=1;
    public SmsOutgoingObserver(Handler handler)
    {
        super(handler);
    }

    @Override
    public void onChange(boolean selfChange)
    {
        Cursor cursor = mainActivity.getContentResolver().query(
                Uri.parse("content://sms/sent/"), null, null, null, null);
        if (cursor.moveToNext())
        {
            String protocol = cursor.getString(cursor.getColumnIndex("protocol"));
            int type = cursor.getInt(cursor.getColumnIndex("type"));
            // Only processing outgoing sms event & only when it
            // is sent successfully (available in SENT box).
            if (protocol != null || type != MESSAGE_TYPE_SENT) {
                return;
            }
            int dateColumn = cursor.getColumnIndex("date");
            int bodyColumn = cursor.getColumnIndex("body");
            int addressColumn = cursor.getColumnIndex("address");
            String from = "0";
            String to = cursor.getString(addressColumn);
            String message = cursor.getString(bodyColumn);
        }
    }

   public void registrar()
   {
       String date = getShared(mainActivity, LAST_DATE_READ_INBOX, "0");

   }





}
