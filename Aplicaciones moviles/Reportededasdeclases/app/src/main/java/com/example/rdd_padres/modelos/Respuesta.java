package com.example.rdd_padres.modelos;

public class Respuesta
{
    private String uri;
    private int estado=0;

    public String getUri()
    {
        return uri;
    }

    public int getEstado() {
        return estado;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
}
