package com.example.rdd_padres.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.rdd_padres.R;
import com.example.rdd_padres.adapter.AdapterReporte;
import com.example.rdd_padres.commons;
import com.example.rdd_padres.modelos.Reporte;

import java.util.ArrayList;

import static com.example.rdd_padres.Manejador_BD.Excecute;
import static com.example.rdd_padres.catalogos.COD_ESTADO_ENVIADO;

public class reportesEnviados extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reportes_enviados);

        initComponents();
    }

    private void initComponents()
    {
        setLista();
        setButtons();
    }

    private void setButtons()
    {
        /*boton liberar espacio*/
        ((TextView) this.findViewById(R.id.btn_liberar)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                //liberarEspacio();
                commons.showMessageDialog(commons.menuReportes,"Tarea completada!","Se ha limpiado el espacio de su teléfono",R.drawable.ic_trash,null);
                Excecute();
                finish();
            }
        });


    }

    private void setLista()
    {
        ArrayList<Reporte> reportes = commons.BD.getReportes("SELECT * FROM REPORTE WHERE ESTADO = "+COD_ESTADO_ENVIADO+";");

        if(reportes==null || reportes.size()==0)
        {
            commons.showAlertDialog(
                    commons.menuReportes,
                    "Alerta",
                    "No cuenta con reportes previamente enviados",R.drawable.ic_error_outline,null);
            finish();
        }
        RecyclerView recList;
        AdapterReporte ca;

        recList = findViewById(R.id.Lista);
        //recList.setOnClickListener(this);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        registerForContextMenu(recList);
        ca = new AdapterReporte(reportes);
        recList.setAdapter(ca);
    }
}