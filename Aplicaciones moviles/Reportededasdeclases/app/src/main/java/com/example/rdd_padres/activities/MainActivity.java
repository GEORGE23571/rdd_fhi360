package com.example.rdd_padres.activities;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import com.example.rdd_padres.R;
import com.example.rdd_padres.commons;
import com.example.rdd_padres.utils.SmsOutgoingObserver;
import com.google.android.gms.location.FusedLocationProviderClient;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

import static com.example.rdd_padres.catalogos.initCatalogos;
import static com.example.rdd_padres.commons.getParametros;
import static com.example.rdd_padres.commons.initBD;
import static com.example.rdd_padres.commons.mainActivity;
import static com.example.rdd_padres.commons.longitud;
import static com.example.rdd_padres.commons.latitud;

public class MainActivity extends AppCompatActivity {

    private int MY_PERMISSIONS_REQUEST_SMS_RECEIVE = 10;
    private static final int LOCATION_REQUEST = 101;
    private FusedLocationProviderClient fusedLocationClient;
    LocationManager locationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initCatalogos();
        setButtons();
        initBD(getApplicationContext());
        mainActivity = MainActivity.this;

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.RECEIVE_SMS,
                        Manifest.permission.READ_SMS,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        getParametros();
        //setReporteSMS(getApplicationContext(),BANDEJA_ENVIADOS);
        //sendReportesSMS(getApplicationContext());

        ContentResolver contentResolver = getApplicationContext().getContentResolver();
        contentResolver.registerContentObserver(Uri.parse("content://sms/"), true, new SmsOutgoingObserver(new Handler()));



        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST);

            return;
        }else
            {
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER, 1000, 50, locationListenerGPS);

                locationManager.requestLocationUpdates( LocationManager.NETWORK_PROVIDER,  1000, 50, locationListenerNetwork);
            }

    }

    @Override
    public void onResume()
    {
        super.onResume();
        //debugShared();
    }

    private void setButtons(){
        /*REPORTAR*/
        (findViewById(R.id.btn_reportar)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commons.startActivity(getApplicationContext(), reportarDia.class);
            }
        });

        /*REPORTES LLENADOS*/
        (findViewById(R.id.btn_llenados)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                commons.startActivity(getApplicationContext(), menuReportes.class);
            }
        });

        /*CONFIGURACION*/
        (findViewById(R.id.btn_configuracion)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commons.startActivity(getApplicationContext(), configuracion.class);
            }
        });

        /*SALIR*/
        (findViewById(R.id.btn_salir)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home)
        {
            finish();
        }
        else
        {
            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }



    private final LocationListener locationListenerBest = new LocationListener()
    {
        public void onLocationChanged(Location location) {
            longitud = location.getLongitude();
            latitud = location.getLatitude();

            runOnUiThread(new Runnable() {
                @Override
                public void run()
                {
                    //Toast.makeText(MainActivity.this, "Best Provider update "+longitudeBest+"  "+latitudeBest, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {
        }

        @Override
        public void onProviderDisabled(String s) {
        }
    };

    private final LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitud = location.getLongitude();
            latitud = location.getLatitude();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                  //  Toast.makeText(MainActivity.this, "Network Provider update "+longitud+" "+latitud, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {

        }
        @Override
        public void onProviderDisabled(String s) {

        }
    };

    private final LocationListener locationListenerGPS = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitud = location.getLongitude();
            latitud = location.getLatitude();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //Toast.makeText(MainActivity.this, "GPS Provider update" +longitud+" "+latitud , Toast.LENGTH_SHORT).show();
                }
            });
        }
        @Override
        public void onStatusChanged(String s, int i, Bundle bundle)
        {
        }

        @Override
        public void onProviderEnabled(String s) {
        }
        @Override
        public void onProviderDisabled(String s) {
        }
    };
}