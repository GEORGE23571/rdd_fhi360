package com.example.rdd_padres.modelos;

public class Sms
{
    private String _id;
    private String date;
    private String body;

    public Sms (String _id_, String date_ , String body)
    {
        this._id=_id_;
        this.date=date_;
        this.body=body;
    }

    public String get_id() {
        return _id;
    }

    public String getDate() {
        return date;
    }

    public String getBody() {
        return body;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setBody(String doby) {
        this.body = doby;
    }
}
