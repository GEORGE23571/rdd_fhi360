package com.example.rdd_padres.modelos;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.rdd_padres.catalogos.listaAcciones;
import static com.example.rdd_padres.catalogos.listaGrados;
import static com.example.rdd_padres.catalogos.listaMaterias;


public class Reporte
{
    private String centro;
    private String nombre;
    private String actividad;
    private String fecha;
    private String grado;
    private String latitud;
    private String longitud;
    private String time_start;
    private String time_end;
    private String uri= java.util.UUID.randomUUID().toString();
    private List<String> materias=new ArrayList<>();
    private int tipo_envio;
    private int perdidos;
    private int id;

    public Reporte()
    {
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public void setTime_start(String time_start) {
        this.time_start = time_start;
    }

    public void setTime_end(String time_end) {
        this.time_end = time_end;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public void setMaterias(List<String> materias) {
        this.materias = materias;
    }

    public String getLatitud() {
        return latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public String getTime_start() {
        return time_start;
    }

    public String getTime_end() {
        return time_end;
    }

    public List<String> getMaterias() {
        return materias;
    }

    public int getId() {
        return id;
    }
    public void setId(int id_) {
        this.id=id_;
    }

    public String getUri() {
        return uri;
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCentro() {
        return centro;
    }

    public void setCentro(String centro) {
        this.centro = centro;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getTipo_envio() {
        return tipo_envio;
    }

    public void setTipo_envio(int tipo_envio) {
        this.tipo_envio = tipo_envio;
    }

    public int getPerdidos() {
        return perdidos;
    }

    public void setPerdidos(int perdidos) {
        this.perdidos = perdidos;
    }

    public void addMateria(String materia)
    {
        materias.add(materia);
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getActividad() {
        return actividad;
    }

    public String getGrado() {
        return grado;
    }

    public void setGrado(String grado)
    {
    this.grado = grado;
    }

    public JSONObject getJsonFromReporte()
    {
        JSONObject message = new JSONObject();

        try
        {
            message.put("centro", centro);
            message.put("fecha", fecha);
            message.put("actividad", actividad);
            message.put("uri", uri);
            message.put("latitud", latitud);
            message.put("longitud", longitud);

            if (actividad.equals( listaAcciones.getODKCod(2)))
            {
                    message.put("grado", grado);
                     String cadena ="";

                    for (String materia : materias)
                    {
                        cadena =cadena+" "+materia;
                    }
                    cadena.trim();
                        message.put("materias", cadena);
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return message;
    }
    public JSONObject getJsonFromReporteSMS()
    {
        JSONObject message = new JSONObject();
        try {
            message.put("A", centro);
            message.put("B", fecha);
            message.put("C",listaAcciones.getCodSMS( actividad));

            if (actividad.equals(   listaAcciones.getODKCod(2)))
            {
                message.put("D",listaGrados.getCodSMS( grado));
                String cadena ="";
                for (String materia : materias)
                {
                    cadena =cadena+" "+listaMaterias.getCodSMS( materia);
                }
                cadena.trim();
                message.put("E", cadena);
            }

            return message;
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return null;
    }


}
