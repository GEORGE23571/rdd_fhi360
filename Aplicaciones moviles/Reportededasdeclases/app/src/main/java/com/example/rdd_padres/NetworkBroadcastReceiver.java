package com.example.rdd_padres;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

import static com.example.rdd_padres.commons.getParametros;
import static com.example.rdd_padres.commons.initBD;
import static com.example.rdd_padres.commons.mainActivity;
import static com.example.rdd_padres.commons.sendReportesSMS;
import static com.example.rdd_padres.commons.validateInternetConnection;
import static com.example.rdd_padres.utils.sendReporte.enviar_internet;

public class NetworkBroadcastReceiver extends BroadcastReceiver
{
    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private static final String SMS_SENT ="";
    private static final String CONNECTIVITY_CHANGE = "android.net.conn.CONNECTIVITY_CHANGE";
    private static final String TAG = "SMSBroadcastReceiver";
    private static  boolean estado=false;


    public void onReceive(final Context context, final Intent intent)
    {
        /*Toast toast = Toast.makeText(context,intent.getAction().toString()  , Toast.LENGTH_LONG);
        toast.show();
        if (intent.getAction() == SMS_RECEIVED)
        {
            Bundle bundle = intent.getExtras();
            if (bundle != null)
            {
                Object[] pdus = (Object[])bundle.get("pdus");
                final SmsMessage[] messages = new SmsMessage[pdus.length];
                for (int i = 0; i < pdus.length; i++)
                {
                    messages[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                }
                if (messages.length > -1)
                {
                    sendReportesSMS(context);
                }
            }
        }
        else if (intent.getAction() == CONNECTIVITY_CHANGE )
        {
            if( validateInternetConnection(context)&& estado==false)
            {
                initBD( context);
                estado=true;
                getParametros();
                enviar_internet(mainActivity, false);
            }
            else
            {
                estado=false;
            }
        }
        else
        {
        }*/
    }

    public static SmsMessage[] getMessagesFromIntent(Intent intent)
    {
        Object[] messages = (Object[]) intent.getSerializableExtra("pdus");
        byte[][] pduObjs = new byte[messages.length][];

        for (int i = 0; i < messages.length; i++) {
            pduObjs[i] = (byte[]) messages[i];
        }
        byte[][] pdus = new byte[pduObjs.length][];
        int pduCount = pdus.length;
        SmsMessage[] msgs = new SmsMessage[pduCount];
        for (int i = 0; i < pduCount; i++)
        {
            pdus[i] = pduObjs[i];
            msgs[i] = SmsMessage.createFromPdu(pdus[i]);
        }
        return msgs;
    }
}