package com.example.rdd_padres.fragmentos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.rdd_padres.R;
import com.example.rdd_padres.commons;

public class selectFecha extends Fragment
{
    public static int STEP=2;
    public View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable  ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_select_fecha, container, false);
        commons.pushFragment(this);
        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        commons.reportarDia.setCurrentStep(this.STEP);
    }



    public static void next(){
        /*store date*/
        View view = ((selectFecha)commons.getCurrentFragment()).view;

        DatePicker datePicker =  view.findViewById(R.id.date_picker);

        String month ="";
        String day ="";
        if (datePicker.getMonth()+1<10)
        {
            month="0"+(datePicker.getMonth()+1);
        }
        else
            {
                month =""+(datePicker.getMonth()+1);
            }

        if (datePicker.getDayOfMonth()<10)
        {
            day="0"+datePicker.getDayOfMonth();
        }
        else
        {
            day =""+datePicker.getDayOfMonth();
        }

        commons.reporte.setFecha(datePicker.getYear()+"-"+month+"-"+day);

        /*do next*/
        FragmentTransaction ft = commons.getCurrentFragment().getFragmentManager().beginTransaction();
        ft.replace(R.id.pager, new selectCaso()).addToBackStack("caso");
        ft.commit();
    }


}
