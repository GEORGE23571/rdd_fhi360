package com.example.rdd_padres.utils;
import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;

import com.example.rdd_padres.R;

public  class Enrutador implements
        LocationListener
{
    private static final int LOCATION_REQUEST = 101;


    public static String get_coordenadas(Activity activity)
    {//retorna la posicion actual en el formato 'latitud,longitud'
        ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setIcon(R.mipmap.ic_launcher);

        progressDialog.setMessage("Obteniendo coordenadas ...");
        progressDialog.show();

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        final LocationManager locMgr = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        final String provider = locMgr.getBestProvider(criteria, true);
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST);
            return "";
        }
        locMgr.requestLocationUpdates(provider, 1000, 100, new Enrutador());
        Location loc =  locMgr.getLastKnownLocation(provider);
        progressDialog.dismiss();
        return  loc.getLatitude()+","+loc.getLongitude();
    }

    @Override
    public void onLocationChanged(Location location) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onProviderDisabled(String provider) {}
}

