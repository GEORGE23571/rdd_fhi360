package com.example.rdd_padres.fragmentos;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.rdd_padres.R;
import com.example.rdd_padres.activities.reportesSinEnviar;
import com.example.rdd_padres.commons;

import static com.example.rdd_padres.catalogos.*;
import static com.example.rdd_padres.utils.sendReporte.*;
import static com.example.rdd_padres.commons.*;

public class selectEnvio extends Fragment
{
    public static int STEP = 5;

    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_select_envio, container, false);
        commons.pushFragment(this);
        initComponents();
        commons.reporte.setLatitud(""+latitud);
        commons.reporte.setLongitud(""+longitud);
        return view;
    }
    @Override
    public void onResume()
    {
        super.onResume();
        commons.reportarDia.setCurrentStep(this.STEP);
    }

    private void initComponents()
    {
        setButtons();
        lastStep=getActivity();
    }

    private void setButtons()
    {
        //INTERNET
        view.findViewById(R.id.btn_internet).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view)
            {
                commons.reporte.setTipo_envio(COD_ENVIO_INTERNET);
                enviar(getActivity(),true);
                lastStep.finish();
                if (validateInternetConnection(mainActivity))
                {
                    commons.showMessageDialog(mainActivity, "¡Reporte enviado!",
                            "Se enviaron su repuestas, puede verificar el estado en reportes llenados ",
                            R.drawable.ic_enviados, null);
                    enviar_internet(mainActivity, true);

                }
                else
                {
                    commons.showAlertDialog(mainActivity, "Sin conexión!",
                            "El dispositivo no cuenta con una conexión a internet en este momento. Su reporte se enviará cuando se cuente con conexión a internet",
                            R.drawable.ic_signal_wifi_off, null);
                }

            }
        });

        //SMS
        view.findViewById(R.id.btn_sms).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view)
            {

                if (checkSmsPermission(getActivity()))
                {
                    commons.reporte.setTipo_envio(COD_ENVIO_SMS);
                    enviar(getActivity(),true);
                    lastStep.finish();
                    enviar_sms(mainActivity, true);

                }
                else
                {

                }
            }
        });
    }
}
