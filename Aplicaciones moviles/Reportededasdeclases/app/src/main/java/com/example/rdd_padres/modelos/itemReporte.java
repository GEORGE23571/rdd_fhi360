package com.example.rdd_padres.modelos;

import java.util.ArrayList;

public class itemReporte
{
    ArrayList<item> lista = new ArrayList<>();

    public ArrayList<item> getLista()
    {
        return lista;
    }

    public void setLista(ArrayList<item> lista) {
        this.lista = lista;
    }

    public itemReporte()
    {

    }

    public String  getODKCod(int i )
    {
        return lista.get(i).getCod_ODK();
    }

    public String getODKcod(String value)
    {
         for(item e: lista)
         {
             if(e.getNombre().equals(value))
             {
                 e.getCod_ODK();
             }

         }
       return "grado erroneo";
    }
    public int getCodSMS(String codODK)
    {
        for (item e: lista)
        {
            if (e.getCod_ODK().equals(codODK))
            {
                return e.getCod_SMS();
            }
        }
        return -1;
    }

    public String gettitulo(String codODK)
    {
        for (item e: lista)
        {
            if (e.getCod_ODK().equals(codODK))
            {
                return e.getNombre();
            }
        }
        return "-1";
    }

    public String decodeSMStoODK(int codSMS)
    {
        for (item e: lista)
        {
            if (e.getCod_SMS()==codSMS)
            {
                return e.getCod_ODK();
            }
        }
        return "-1";
    }

    public String getCodODK(String titulo)
    {
        for (item e: lista)
        {
            if (e.getNombre().equals(titulo))
            {
                return e.getCod_ODK();
            }
        }
        return "-1";
    }

    public void  add(String nombre_, String cod_ODK_, int codSMS_)
    {
        lista.add(new item( nombre_, cod_ODK_, codSMS_));
    }

    public ArrayList<String> titulos()
    {
        ArrayList<String> l = new ArrayList<>();

        for(item e: lista)
        {
            l.add(e.getNombre());
        }
        return l;
    }

    public class item
    {
        private String nombre;
        private String cod_ODK;
        private int cod_SMS;

        public item(String nombre_, String cod_ODK_, int codSMS_)
        {
            this.nombre=nombre_;
            this.cod_ODK=cod_ODK_;
            this.cod_SMS=codSMS_;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public void setCod_ODK(String cod_ODK) {
            this.cod_ODK = cod_ODK;
        }

        public void setCod_SMS(int cod_SMS) {
            this.cod_SMS = cod_SMS;
        }

        public String getNombre() {
            return nombre;
        }

        public String getCod_ODK() {
            return cod_ODK;
        }

        public int getCod_SMS() {
            return cod_SMS;
        }
    }
}
