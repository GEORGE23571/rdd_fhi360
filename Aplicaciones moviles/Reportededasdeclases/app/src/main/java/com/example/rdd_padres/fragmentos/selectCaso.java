package com.example.rdd_padres.fragmentos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import com.example.rdd_padres.R;
import com.example.rdd_padres.commons;

import static com.example.rdd_padres.catalogos.listaAcciones;


public class selectCaso extends Fragment {
    public static int STEP=3;
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_select_caso, container, false);
        commons.pushFragment(this);
        setButtons();
        return view;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        commons.reportarDia.setCurrentStep(this.STEP);
    }

    private void setButtons(){
        //CENTRO CERRADO
        view.findViewById(R.id.btn_cerrado).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commons.delay(150);
                commons.reporte.setActividad(listaAcciones.getODKCod(1));
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.pager, new selectEnvio()).addToBackStack("envio");
                ft.commit();
            }
        });
        //NORMALES
        view.findViewById(R.id.btn_normales).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commons.delay(150);
                commons.reporte.setActividad(listaAcciones.getODKCod(0));
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.pager, new selectEnvio()).addToBackStack("envio");
                ft.commit();
            }
        });
        //CIERTAS MATERIAS
        view.findViewById(R.id.btn_ciertas_materias).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                commons.delay(150);
                commons.reporte.setActividad(listaAcciones.getODKCod(2));
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.pager, new selectMaterias()).addToBackStack("Ciertas materias");
                ft.commit();
            }
        });

    }


}
