package com.example.rdd_padres.modelos;

public class Nivel
{
    private String ID_NIVEL;
    private String NOMBRE;
    private String COD_ESTADO;

    public String getID_NIVEL() {
        return ID_NIVEL;
    }

    public void setID_NIVEL(String ID_NIVEL) {
        this.ID_NIVEL = ID_NIVEL;
    }

    public String getNOMBRE() {
        return NOMBRE;
    }

    public void setNOMBRE(String NOMBRE) {
        this.NOMBRE = NOMBRE;
    }

    public String getCOD_ESTADO() {
        return COD_ESTADO;
    }

    public void setCOD_ESTADO(String COD_ESTADO) {
        this.COD_ESTADO = COD_ESTADO;
    }
}
