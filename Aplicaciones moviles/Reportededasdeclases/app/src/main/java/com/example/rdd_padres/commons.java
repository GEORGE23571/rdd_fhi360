package com.example.rdd_padres;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.*;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.rdd_padres.activities.MainActivity;
import com.example.rdd_padres.activities.busquedaCentro;
import com.example.rdd_padres.activities.reportarDia;
import com.example.rdd_padres.modelos.Establecimiento;
import com.example.rdd_padres.modelos.Reporte;
import com.example.rdd_padres.modelos.Sms;
import com.google.gson.Gson;
import com.google.gson.JsonArray;

import static com.example.rdd_padres.catalogos.BANDEJA_RECIBIDOS;
import static com.example.rdd_padres.catalogos.COD_ENVIO_INTERNET;
import static com.example.rdd_padres.catalogos.LAST_DATE_READ_INBOX;
import static com.example.rdd_padres.catalogos.listaAcciones;
import static com.example.rdd_padres.catalogos.listaGrados;
import static com.example.rdd_padres.catalogos.listaMaterias;
import static com.example.rdd_padres.utils.sendReporte.enviar_internet;

public class commons
{
    public static final String RDDAPI_URL = "http://3.14.150.185:8080/RDD/wsApp/";
    //public static final String RDDAPI_URL = "http://192.168.1.7:8080/RDD/wsresources/wsApp/";
    public static Manejador_BD BD;
    public static RequestQueue queue=null;

    public static Activity lastStep;
    public static Activity mainActivity ;

    public static ProgressDialog progressDialog;
    public static Activity menuReportes;

    public static reportarDia reportarDia;
    private static Stack<Fragment> currentFragment;
    public static Reporte reporte;

    public static double longitud, latitud;

    public static final String FAVORITOS = "favoritos";


    public static void setQueue(Context activity)
    {
        if(queue==null)
        {
            queue = Volley.newRequestQueue(activity);
        }
    }

    public static void initBD( Context context)
    {
        try
        {
            BD = new Manejador_BD(context);
            BD.CrearDB();
            BD.openDataBase();
        } catch (Exception e)
        {
        }
    }

    public static void getParametros()
    {
        RequestQueue queue = Volley.newRequestQueue(mainActivity);
        String url = commons.RDDAPI_URL + "getParametros";
        JSONArray jsonBody = new JSONArray();
        JsonArrayRequest request = new JsonArrayRequest
                (Request.Method.GET, url, jsonBody, new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response)
                    {
                        try
                        {
                            int len = response.length();
                            for (int i = 0; i < len; i++)
                            {
                                JSONObject JSONPARAM =  response.getJSONObject(i);
                                putShared(mainActivity,JSONPARAM.getString("name"),JSONPARAM.getString("valor"));
                            }
                        }
                        catch (Throwable tx)
                        {
                            tx.printStackTrace();
                        }
                    }

                },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error)
                            {

                                error.printStackTrace();
                            }
                        });
        queue.add(request);
    }

    /*UTILITIES*/
    public static void startActivity(Context context, Class c)
    {
        Intent i = new Intent(context, c);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    public static void delay(int del)
    {
        try
        {
            Thread.sleep(del);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    /*END UTILITIES*/

    /*ESTABLECIMIENTOS FAVORITOS*/
    private static ArrayList<Establecimiento> establecimientos_favoritos;

    public static ArrayList<Establecimiento> getEstablecimientosFavoritos(Context activity)
    {
        establecimientos_favoritos = new ArrayList<>();
        String favoritos = getShared(activity, FAVORITOS, null);
        if (favoritos == null || favoritos.equals("")) return null;

        try {
            JSONArray jsonEstablecimientos = new JSONArray(favoritos);
            int len = jsonEstablecimientos.length();
            for (int i = 0; i < len; i++) {
                JSONObject jsonEstablecimiento = jsonEstablecimientos.getJSONObject(i);
                establecimientos_favoritos.add(Establecimiento.newFromJSON(jsonEstablecimiento));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return establecimientos_favoritos;
    }

    public static void addEstablecimientoFavorito(Context activity, Establecimiento e) {
        String establecimientosString = "[]";
        String establecimientos = getShared(activity, FAVORITOS, null);
        if (establecimientos == null || establecimientos.equals("")) {
            establecimientosString = "[" + e.toJSONString() + "]";
        } else {
            try {
                JSONArray jsonEstablecimientos = new JSONArray(establecimientos);
                jsonEstablecimientos.put(e.toJSON());
                establecimientosString = jsonEstablecimientos.toString();
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
        putShared(activity, FAVORITOS, establecimientosString);
    }
    public static void removeEstablecimientoFavorito(Context activity, String codigo) {
        int targetIndex = 0;
        for (Establecimiento e : establecimientos_favoritos) {
            if (codigo.equals(e.getCodigo())) {
                break;
            }
            targetIndex++;
        }

        establecimientos_favoritos.remove(targetIndex);

        /*COMIT CHANGES TO SHARED PREFERENCES*/
        JSONArray jsonEstablecimientos = new JSONArray();
        for (Establecimiento e : establecimientos_favoritos) {
            jsonEstablecimientos.put(e.toJSON());
        }
        String establecimientosString = jsonEstablecimientos.toString();

        putShared(activity, FAVORITOS, establecimientosString);
    }
    /*END FAVORITOS*/

    /*REPORTE*/


    public static void pushFragment(Fragment fragment) {
        currentFragment.push(fragment);
    }
    public static Fragment getCurrentFragment()
    {
        return currentFragment.get(currentFragment.size()-1);
    }

    public static void initReporte(com.example.rdd_padres.activities.reportarDia instance)
    {
        commons.reportarDia = instance;
        currentFragment = new Stack<>();
    }
    /*END REPORTE*/

    public static void showAlertDialog(final Activity activity, String title, String mensaje, int iconId, String textoBoton)
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();

        View v = inflater.inflate(R.layout.dialog_accept, null);

        Button btn = v.findViewById(R.id.bt_close);
        //TITULO
        TextView titleView = v.findViewById(R.id.title);
        titleView.setText(title);
        //CONTENT
        TextView content = v.findViewById(R.id.content);
        content.setText(mensaje);
        //ICONO
        ImageView icon = v.findViewById(R.id.icon);
        icon.setImageResource(iconId);
        //TEXTO DEL BOTON
        if(textoBoton!=null){
            btn.setText(textoBoton);
        }
        v.findViewById(R.id.header).setBackgroundResource(R.color.red_300);
        v.findViewById(R.id.bt_close).setBackgroundResource(R.drawable.btn_rounded_red);
        builder.setView(v);
        final AlertDialog alerta = builder.create();
        alerta.show();
        btn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        alerta.dismiss();
                    }
                }

        );
    }

    public static void showMessageDialog(Activity activity,String title, String mensaje, int iconId, String textoBoton)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();

        View v = inflater.inflate(R.layout.dialog_accept, null);


        Button btn = v.findViewById(R.id.bt_close);
        //TITULO
        TextView titleView = v.findViewById(R.id.title);
        titleView.setText(title);
        //CONTENT
        TextView content = v.findViewById(R.id.content);
        content.setText(mensaje);
        //ICONO
        ImageView icon = v.findViewById(R.id.icon);
        icon.setImageResource(iconId);
        //TEXTO DEL BOTON
        if(textoBoton!=null){
            btn.setText(textoBoton);
        }

        builder.setView(v);
        final AlertDialog alerta = builder.create();
        alerta.show();
        btn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        alerta.dismiss();
                    }
                }

        );
    }

    public static void showProgresBar(String texto, Context context )
    {
        progressDialog= new ProgressDialog(context);
        progressDialog.setMessage(texto);
        progressDialog.show();
    }
    public static  void hideProgressBar()
    {
        progressDialog.dismiss();
    }

    /*SHARED PREFERENCES*/


    public static String getShared(Context activity, String key, String defValue)
    {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity);
        return sp.getString(key, defValue);
    }
    public static void putShared(Context activity, String key, String value)
    {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key, value);
        editor.apply();
    }
    /* END SHARED PREFERENCES*/


    public static  boolean validateInternetConnection(Context activity)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (!(networkInfo != null && networkInfo.isConnected()))
        {
            return false;
        }
        return true;
    }


    public static boolean checkSmsPermission(Activity activity)
    {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.SEND_SMS))
            {
            }
            else
            {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.SEND_SMS}, 0);
            }

            return false;
        }
        return true;
    }

    public static ArrayList<Sms> redSMS(Context activity, String bandeja, String date)
    {
        ArrayList<Sms> list = new ArrayList<>();

        if (!checkSmsPermission(mainActivity))
        {
            return null;
        }

        final String[] projection = new String[]{"_id","date", "body"};

        String where_clause = "body like '%(\"A\":\"%'";

        if (!date.equals(""))
        {
            where_clause+=" and  date > "+date;
        }

        Cursor cur;
        try
        {

            cur =activity.getContentResolver().query(Uri.parse(bandeja), projection ,where_clause  , null, null);

            if (cur.moveToFirst())
            {
                do {
                    list.add(new Sms(cur.getString(0),cur.getString(1),cur.getString(2)));
                }
                while (cur.moveToNext());
            }
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return list;
    }

    public static void sendReportesSMS(Context context)
    {
        if (!checkSmsPermission(mainActivity))
        {
            return ;
        }

        String date = getShared(context, LAST_DATE_READ_INBOX, "0");
        long date_final= Long.parseLong(date);

        ArrayList<Sms> listaSms = redSMS(context,BANDEJA_RECIBIDOS, date);
        System.out.println("*** cantidad de mensjaes recuperados "+listaSms.size());
        Gson gson = new Gson();

        for (Sms mensaje:listaSms)
        {
            Long date_sms =Long.parseLong(mensaje.getDate());

            if (date_sms>date_final)
            {
                date_final=date_sms;
            }

            String[] reportes = mensaje.getBody().split(";");
            for (String reporteSMS: reportes)
            {
                System.out.println("reporte de mensaje "+ reporteSMS);
                reporteSMS= reporteSMS.replace("(","{");
                reporteSMS= reporteSMS.replace(")","}");

                try
                {
                    JSONObject jsonSMS = new JSONObject(reporteSMS);
                    JSONObject jsonReporte = new JSONObject();

                    String codigo = jsonSMS .getString("A");
                    String fecha = jsonSMS .getString("B");
                    int Accion = jsonSMS.getInt("C");

                    jsonReporte.put("centro",codigo);
                    jsonReporte.put("fecha",fecha);
                    jsonReporte.put("actividad",listaAcciones.decodeSMStoODK(Accion));


                    if(jsonSMS.has("D"))
                    {
                        int grado = jsonSMS .getInt("D");
                        jsonReporte.put("grado",listaGrados.decodeSMStoODK(grado));
                    }

                    Reporte reporte = gson.fromJson(jsonReporte.toString(),Reporte.class);
                    if(jsonSMS.has("D"))
                    {
                        String[] Materia = jsonSMS .getString("E").trim().split(" ");
                        for (int i =0;i<Materia.length;i++)
                        {
                            reporte.addMateria(listaMaterias.decodeSMStoODK(Integer.parseInt(Materia[i])));
                        }

                    }

                    reporte.setTipo_envio(COD_ENVIO_INTERNET);
                    BD.setReporte(reporte);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }
        enviar_internet(mainActivity, false);
        putShared(context,LAST_DATE_READ_INBOX,""+ date_final);
    }


    public static class CheckNetwork {

        private static Context context;

        public CheckNetwork(Context context_) {
            context = context_;
        }

        // Network Check
        @SuppressLint("NewApi")
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        public void registerNetworkCallback() {
            try {
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkRequest.Builder builder = new NetworkRequest.Builder();

                connectivityManager.registerDefaultNetworkCallback(new ConnectivityManager.NetworkCallback() {
                                                                       @Override
                                                                       public void onAvailable(Network network)
                                                                       {
                                                                           Variables.isNetworkConnected = true; // Global Static Variable
                                                                       }

                                                                       @Override
                                                                       public void onLost(Network network) {
                                                                           Variables.isNetworkConnected = false; // Global Static Variable
                                                                       }
                                                                   }

                );
                Variables.isNetworkConnected = false;
            } catch (Exception e) {
                Variables.isNetworkConnected = false;
            }
        }
    }

    public static class Variables
    {
        public static boolean isNetworkConnected = false;
    }
}
