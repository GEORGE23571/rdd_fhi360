package com.example.rdd_padres.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.rdd_padres.R;
import com.example.rdd_padres.adapter.AdapterReporte;
import com.example.rdd_padres.commons;
import com.example.rdd_padres.modelos.Reporte;

import java.util.ArrayList;

import static com.example.rdd_padres.catalogos.COD_ESTADO_SIN_ENVIAR;
import static com.example.rdd_padres.commons.validateInternetConnection;
import static com.example.rdd_padres.utils.sendReporte.enviar_internet;
import static com.example.rdd_padres.commons.lastStep;
import static com.example.rdd_padres.utils.sendReporte.enviar_sms;

public class reportesSinEnviar extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reportes_sin_enviar);
        setLista();
        setButtons();
    }
    private void setButtons()
    {
        //enviar_ahora= findViewById(R.id.btn_liberar);
        //enviar_ahora.setOnClickListener(this);

        ((TextView) this.findViewById(R.id.btn_enviar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                lastStep=reportesSinEnviar.this;


                if (validateInternetConnection(reportesSinEnviar.this))
                {
                    enviar_internet(reportesSinEnviar.this, false);
                    commons.showMessageDialog(commons.menuReportes, "¡Reporte enviado!",
                            "Se enviaron su repuestas, puede verificar el estado en reportes llenados ",
                            R.drawable.ic_enviados, null);
                }
                else
                {
                   if( enviar_sms(reportesSinEnviar.this, false))
                   {
                       commons.showMessageDialog(commons.menuReportes, "Reportes enviados!",
                               "Se han enviado los reportes por mensaje de texto, puede verificar el estado en reportes llenados",
                               R.drawable.ic_enviados, null);
                   }
                   else
                       {
                           commons.showAlertDialog(commons.menuReportes, "Sin conexión!",
                                   "El dispositivo no cuenta con una conexión a internet en este momento. Su reporte se enviará cuando se cuente con conexión a internet",
                                   R.drawable.ic_signal_wifi_off, null);
                       }
                }
                finish();
            }
        });
    }

    private void setLista()
    {
        ArrayList<Reporte> reportes = commons.BD.getReportes("SELECT * FROM REPORTE WHERE ESTADO = "+COD_ESTADO_SIN_ENVIAR+";");
        if(reportes==null || reportes.size()==0)
        {
            commons.showAlertDialog(commons.menuReportes ,"Alerta","No cuenta con reportes sin enviar",R.drawable.ic_error_outline,null);
            finish();

        }

        RecyclerView recList;
        AdapterReporte ca;

        recList = findViewById(R.id.Lista);
        //recList.setOnClickListener(this);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        registerForContextMenu(recList);

        ca = new AdapterReporte(reportes);
        recList.setAdapter(ca);
    }
}