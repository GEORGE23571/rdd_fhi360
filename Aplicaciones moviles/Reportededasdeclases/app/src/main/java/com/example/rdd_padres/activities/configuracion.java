package com.example.rdd_padres.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.rdd_padres.R;
import com.example.rdd_padres.adapter.AdapterCentroConfiguracion;
import com.example.rdd_padres.commons;
import com.example.rdd_padres.modelos.Establecimiento;

import java.util.ArrayList;
import java.util.List;

import static com.example.rdd_padres.commons.lastStep;
import static com.example.rdd_padres.commons.validateInternetConnection;

public class configuracion extends AppCompatActivity
{

    RecyclerView recyclerView;
    public static List<Establecimiento> items;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion);
        lastStep= configuracion.this;
        setLista();
        setButtons();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        setLista();
    }

    private void setButtons()
    {
        /*BUSCAR POR DEPARTAMENTO*/
        (findViewById(R.id.btn_buscar_centro)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (validateInternetConnection(configuracion.this ))
                {
                    commons.startActivity(getApplicationContext(), busquedaCentro.class);
                }
                else
                {
                    commons.showAlertDialog(configuracion.this , "Sin conexión!", "El dispositivo no cuenta con una conexión a internet en este momento.No es posible realizar esta acción.", R.drawable.ic_signal_wifi_off, null);
                }
            }
        });
    }

    private void setLista()
    {
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        items = commons.getEstablecimientosFavoritos(this);
        if(items==null || items.size()==0)
        {
            items=new ArrayList<>();
            recyclerView.setVisibility(View.GONE);
            findViewById(R.id.view_empty).setVisibility(View.VISIBLE);
            return;
        }
        else
        {
            recyclerView.setVisibility(View.VISIBLE);
            findViewById(R.id.view_empty).setVisibility(View.GONE);
        }

        //set data and list adapter
        AdapterCentroConfiguracion mAdapter = new AdapterCentroConfiguracion(items);
        mAdapter.setOnDeleteClick(onDeleteClick);
        recyclerView.setAdapter(mAdapter);
    }

    AdapterCentroConfiguracion.OnDeleteClick onDeleteClick = new AdapterCentroConfiguracion.OnDeleteClick() {
        @Override
        public void onDeleteClick(View view, Establecimiento obj, int position)
        {
            commons.removeEstablecimientoFavorito(getApplicationContext(),obj.getCodigo());
            setLista();
            Toast.makeText(getApplicationContext(),obj.getNombre(),Toast.LENGTH_SHORT).show();
        }
    };
}