package com.example.rdd_padres.fragmentos;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rdd_padres.modelos.itemReporte;
import com.google.android.material.snackbar.Snackbar;
import com.example.rdd_padres.R;
import com.example.rdd_padres.adapter.AdapterMateria;
import com.example.rdd_padres.commons;
import com.example.rdd_padres.modelos.Materia;
import java.util.ArrayList;
import java.util.List;

import static com.example.rdd_padres.catalogos.*;

public class selectMaterias extends Fragment implements View.OnClickListener
{
    public static int STEP = 4;
    public static int INDEX_GRADO=0;
    public static Spinner gradoSpinner;
    public static  String codigo_nivel;
    View view;
    RecyclerView recList;
    static AdapterMateria adapterMateria;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_select_materias, container, false);
        commons.pushFragment(selectMaterias.this);
        initComponents();
        return view;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        commons.reportarDia.setCurrentStep(this.STEP);
        initComponents();
    }

    private void initComponents()
    {
        gradoSpinner= view.findViewById(R.id.gradoSpinner);
        gradoSpinner.setAdapter(null);
        List<String> opcionesGrados = new ArrayList<>();
        opcionesGrados.add("Seleccione grado");

        codigo_nivel= commons.reporte.getCentro().split("-")[3];
        RelativeLayout panel= view.findViewById(R.id.ctrlGrado);
        panel.setVisibility(View.VISIBLE);
        if(codigo_nivel.equals("43"))
        {
            for (itemReporte.item element: listaGrados.getLista())
            {
                if(element.getCod_SMS()>0 && element.getCod_SMS()<7)
                {
                    opcionesGrados.add(element.getNombre());
                }

            }
        }
        else if(codigo_nivel.equals("45"))
        {
            for (itemReporte.item element: listaGrados.getLista())
            {
                if(element.getCod_SMS()>6 && element.getCod_SMS()<10)
                {
                    opcionesGrados.add(element.getNombre());
                }

            }
        }
        else if(codigo_nivel.equals("46"))
        {
            for (itemReporte.item element: listaGrados.getLista())
            {
                if(element.getCod_SMS()>10 )
                {
                    opcionesGrados.add(element.getNombre());
                }

            }

        }else
        {//niveles 41 y 42

            panel.setVisibility(View.GONE);
        }



        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item_white_text, opcionesGrados);

        adapter.setDropDownViewResource(R.layout.code_spinner_layout);

        gradoSpinner.setAdapter(adapter);
        setLista();
    }



    private void setLista()
    {
        ArrayList<Materia> materias = new ArrayList<>();

        for(String materia:listaMaterias.titulos())
        {

            materias.add(new Materia(materia));
        }
        recList = view.findViewById(R.id.ListaMateria);
        //recList.setOnClickListener(this);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        registerForContextMenu(recList);

        adapterMateria = new AdapterMateria(materias);
        recList.setAdapter(adapterMateria);
        recList.setOnClickListener(this);
    }

    public static void next()
    {
        /*store data*/
        View view = ((selectMaterias)commons.getCurrentFragment()).view;
        {
            //grado
            int indexGrado= gradoSpinner.getSelectedItemPosition();

            if (indexGrado==0)
            {

                if (codigo_nivel.equals("41") ||codigo_nivel.equals("42"))
                {

                    commons.reporte.setGrado("default_4142");

                }
                else
                {
                    Snackbar.make(view,"Seleccione un grado para continuar",Snackbar.LENGTH_LONG).show();
                    return;
                }

            }else
            {
                commons.reporte.setGrado(listaGrados.getCodODK(gradoSpinner.getSelectedItem().toString()));

            }

                //GET CNB
                boolean seleccionado = false;

                for(Materia element: adapterMateria.getMateria())
                {
                    if (element.getEstado())
                    {
                        seleccionado=true;
                        commons.reporte.addMateria(listaMaterias.getCodODK(element.getNombre()));
                    }
                }

                if(!seleccionado)
                {
                    Snackbar.make(view,"Seleccione al menos una materia para continuar",Snackbar.LENGTH_LONG).show();
                    return;
                }
        }
        /*do next*/
        FragmentTransaction ft = commons.getCurrentFragment().getFragmentManager().beginTransaction();
        ft.replace(R.id.pager, new selectEnvio()).addToBackStack("envio");
        ft.commit();
    }

    @Override
    public void onClick(View view)
    {
    }
}
