package com.example.rdd_padres.modelos;

import java.util.ArrayList;

public class SmsToSent
{
 private String Body;
 private ArrayList<Integer> id_Reportes = new ArrayList();

   public SmsToSent(String Body_, ArrayList<Integer> idReportes)
   {
       Body=Body_;
       id_Reportes=idReportes;
   }

    public String getBody()
    {
        return Body;
    }

    public ArrayList<Integer> getId_Reportes()
    {
        return id_Reportes;
    }

    public void setBody(String body)
    {
        Body = body;
    }

    public void setId_Reportes(ArrayList<Integer> id_Reportes)
    {
        this.id_Reportes = id_Reportes;
    }

    public void addIdReporte(int id)
    {
        id_Reportes.add(id);
    }
}
