package com.example.rdd_padres.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.rdd_padres.R;
import com.example.rdd_padres.modelos.Materia;

import java.util.List;

public class AdapterMateria extends RecyclerView.Adapter<AdapterMateria.MateriaViewHolder>
{

    private List<Materia> materiaList;

    public List<Materia> getMateria()
    {
        return this.materiaList;
    }

    public AdapterMateria(List<Materia> materiaList)
    {
        this.materiaList= materiaList ;
    }
    @NonNull
    @Override
    public MateriaViewHolder onCreateViewHolder(@NonNull ViewGroup  viewGroup, int viewType)
    {
        View itemView = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.cardview_materia_select,viewGroup,false);
        return new MateriaViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MateriaViewHolder holder, int i)
    {

        Materia materia=materiaList.get(i);
        holder.txtMateria.setText(materia.getNombre());
        holder.sw.setChecked(materia.getEstado());
    }



    @Override
    public int getItemCount()
    {
        return materiaList.size();
    }

    public void setEstado (int i)
    {
        Materia materia = materiaList.get(i);
        materia.changeEstado();
    }

    public  class MateriaViewHolder extends RecyclerView.ViewHolder
    {
        protected TextView txtMateria;
        protected SwitchCompat sw;

        public MateriaViewHolder(final View itemView)
        {
            super(itemView);
            itemView.setClickable(true);
            txtMateria = itemView.findViewById(R.id.txtMateria);
            sw= itemView.findViewById(R.id.sw);


            sw.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    System.out.println("item id : "+ getAdapterPosition());
                    setEstado (getAdapterPosition());
                }
            });

        }



    }
}
