package com.example.rdd_padres;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.rdd_padres.modelos.Establecimiento;
import com.example.rdd_padres.modelos.Nivel;
import com.example.rdd_padres.modelos.Reporte;
import com.example.rdd_padres.modelos.Respuesta;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static com.example.rdd_padres.catalogos.COD_ESTADO_ENVIADO;
import static com.example.rdd_padres.catalogos.COD_ESTADO_SIN_ENVIAR;


public class Manejador_BD extends SQLiteOpenHelper
{
    private static final String DB_NAME = "DB_RDD.db";
    private static  String DB_PATH="";
    private static SQLiteDatabase mDataBase;
    private static final int DB_VERSION = 3;
    private Context mContext=null;

    public Manejador_BD(Context context)
    {
        super(context, DB_NAME, null, DB_VERSION);
        rutaDB(context.getApplicationInfo().dataDir);
        mContext=context;
    }
    public static void rutaDB(String ruta)
    {
        File file = new File(ruta);
        if(file.getName().equals("com.example.rdd_padres"))
        {
            DB_PATH= file.getPath() +"/databases/";
            System.out.println("*** ruta encontrada "+ DB_PATH);
        }
        else {
            File[] archivos = file.listFiles();
            for (File f : archivos) {
                if (f.isDirectory())
                {
                    System.out.println("*** "+f.getPath());
                    rutaDB(f.getPath());
                }
            }
        }
    }

    @Override
    public synchronized  void close()
    {
        if(mDataBase!=null)
            mDataBase.close();
        super.close();
    }


    public void openDataBase() throws SQLException
    {
        // Open the database
        String myPath = DB_PATH + DB_NAME;
        mDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    public void CrearDB()  {
        boolean dbExist= checkDB();

        if(dbExist)
        {
            //la base de datos ya existe
        }
        else
        {
            this.getReadableDatabase();
            try
            {
                copyDB();
            }
            catch(Exception e)
            {
                throw new Error("Error copiando DB");
            }
        }
    }

    public boolean checkDB()
    {
        SQLiteDatabase tempdb=null;
        try
        {
            String path =DB_PATH+DB_NAME;
            tempdb= SQLiteDatabase.openDatabase(path,null,SQLiteDatabase.OPEN_READWRITE);
        }
        catch(Exception e){}

        if(tempdb!=null)
            tempdb.close();
        return tempdb!=null?true:false;
    }
    private void copyDB  ()
    {
        try
        {
            InputStream myInput = mContext.getAssets().open(DB_NAME);
            String outFileName=DB_PATH+DB_NAME;
            OutputStream myOutput=new FileOutputStream(outFileName);
            byte[] buffer =new byte[1024];
            int length;
            while((length = myInput.read(buffer))>0)
            {
                myOutput.write(buffer,0,length);
            }
            myOutput.flush();
            myOutput.close();
            myInput.close();
        }
        catch (Exception e) { }

    }

    public static     void Excecute()
    {
        mDataBase.execSQL("DELETE FROM REPORTE WHERE ESTADO = "+COD_ESTADO_ENVIADO );
    }

    public Cursor select(String query) throws SQLException
    {
        return mDataBase.rawQuery(query,null);
    }

    public void setReporte(Reporte reporte)
    {
        Gson gson = new Gson();
        ContentValues values = new ContentValues();
        values.put("URI",reporte.getUri());
        values.put("JSON",gson.toJson(reporte));
        values.put("ESTADO", COD_ESTADO_SIN_ENVIAR);
        values.put("TIPO_ENVIO",reporte.getTipo_envio());
        mDataBase.insert("REPORTE", null, values);
        System.out.println("cuerpo del reporte "+ gson.toJson(reporte));
    }

    public void setEstadoReporte(ArrayList<Respuesta> listRespuestas)
    {
        for (Respuesta res: listRespuestas)
        {

            ContentValues values = new ContentValues();
            if (res.getEstado()==1)
            {
                values.put("ESTADO", COD_ESTADO_ENVIADO);
            }
            else
            {
                values.put("ESTADO", COD_ESTADO_SIN_ENVIAR);
            }

            mDataBase.update("REPORTE", values ,"URI = '"+res.getUri()+"'    ", null);
        }
    }
    public void setEstadoReporte(String uri)
    {
        ContentValues values = new ContentValues();
        values.put("ESTADO", COD_ESTADO_ENVIADO);
        mDataBase.update("REPORTE", values ,"URI = '"+uri+"'    ", null);
    }
    public void setEstadoReporte(int id,int ESTADO)
    {
        ContentValues values = new ContentValues();
        values.put("ESTADO", ESTADO);
        mDataBase.update("REPORTE", values ,"ID = '"+id+"'    ", null);
    }


    public ArrayList<Reporte> getReportes(String query)
    {
        ArrayList arreglo = new ArrayList<Reporte>();
        Cursor cursor= select (query );
        cursor.moveToFirst();
        Gson gson = new Gson();

        if (cursor.moveToFirst())
        {
            do
            {
                Reporte element = gson.fromJson (cursor.getString(cursor.getColumnIndex("JSON")), Reporte.class);
                element.setId(cursor.getInt(cursor.getColumnIndex("ID")));
                arreglo.add(element);
            } while(cursor.moveToNext());
        }

        cursor.close();
        //ordenar por fecha

        Collections.sort(arreglo, new Comparator()
        {
            @Override
            public int compare(Object o, Object t1)
            {
                return ((Reporte)t1).getFecha().compareTo(((Reporte)o).getFecha());
            }

        });
        return arreglo;
    }

    public ArrayList<Establecimiento> getEstablecimientos(String query)
    {
        ArrayList<Establecimiento>  arreglo = new ArrayList<>();

        Cursor cursor= select (query );
        cursor.moveToFirst();
        System.out.println("consulta a realziar  "+ query);

        if (cursor.moveToFirst())
        {
            do
            {
                Establecimiento element=new Establecimiento();
                element.setCodigo(cursor.getString(cursor.getColumnIndex("ID_DEPARTAMENTO"))+"-"+
                        cursor.getString(cursor.getColumnIndex("ID_MUNICIPIO"))+"-"+
                        cursor.getString(cursor.getColumnIndex("ID_ESTAB"))+"-"+
                        cursor.getString(cursor.getColumnIndex("ID_NIVEL")));

                element.setNombre(cursor.getString(cursor.getColumnIndex("NOMBRE")));
                element.setDireccion(cursor.getString(cursor.getColumnIndex("DIRECCION")));
                element.setJornada(cursor.getString(cursor.getColumnIndex("ID_JORNADA")));
                element.setLatitud(cursor.getDouble(cursor.getColumnIndex("LATITUD")) );
                element.setLongitud(cursor.getDouble(cursor.getColumnIndex("LONGITUD")) );

                arreglo.add(element);
            } while(cursor.moveToNext());
        }

        cursor.close();
        //ordenar por fecha

        Collections.sort(arreglo, new Comparator()
        {
            @Override
            public int compare(Object o, Object t1)
            {
                return 0;
            }

        });
        return arreglo;
    }

    public ArrayList<Nivel> getNiveles(String query)
    {
        ArrayList<Nivel>  arreglo = new ArrayList<>();

        Cursor cursor= select (query );
        cursor.moveToFirst();
        System.out.println("consulta a realziar  "+ query);

        if (cursor.moveToFirst())
        {
            do
            {
                Nivel element=new Nivel();

                element.setID_NIVEL (cursor.getString(cursor.getColumnIndex("ID_NIVEL")));
                element.setNOMBRE(cursor.getString(cursor.getColumnIndex("NOMBRE")));
                element.setCOD_ESTADO(cursor.getString(cursor.getColumnIndex("COD_ESTADO")));

                arreglo.add(element);
            } while(cursor.moveToNext());
        }

        cursor.close();
        //ordenar por fecha

        Collections.sort(arreglo, new Comparator()
        {
            @Override
            public int compare(Object o, Object t1)
            {
                return 0;
            }

        });
        return arreglo;
    }









    @Override
    public void onCreate(SQLiteDatabase db)
    {}

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        if(newVersion>oldVersion)
            copyDB();
    }
}