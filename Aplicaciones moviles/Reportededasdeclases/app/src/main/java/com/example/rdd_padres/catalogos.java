package com.example.rdd_padres;


import com.example.rdd_padres.modelos.itemReporte;

public class catalogos
{

    public static final int MaxLengthSMS=160;

    public static final int COD_ESTADO_ENVIADO =1;
    public static final int COD_ESTADO_SIN_ENVIAR =0;
    public static final int COD_ESTADO_ELIMINADO=-1;

    public static final int COD_ENVIO_SMS =1;
    public static final int COD_ENVIO_INTERNET =2;

    public static final String LAST_DATE_READ_SENT="RDD_LAST_DATE_READ_SENT";
    public static final String LAST_DATE_READ_INBOX="RDD_LAST_DATE_READ_INBOX";
    public static final String LAST_SMS_ID="RDD_LAST_SMS_ID";

    public static final String BANDEJA_ENVIADOS="content://sms/sent/";
    public static final String BANDEJA_RECIBIDOS="content://sms/inbox/";

    public static itemReporte listaAcciones;
    public static itemReporte listaMaterias;
    public static itemReporte listaGrados;

    public static void initCatalogos()
    {
        listaAcciones=new itemReporte();
        listaMaterias=new itemReporte();
        listaGrados=new itemReporte();
        //estados del centro educativo

        listaAcciones.add("Actividades normales", "actividades_normales", 1);
        listaAcciones.add("El centro estaba cerrado", "centro_cerrado", 2);
        listaAcciones.add("No se impartieron materias", "no_se_impartieron_materias", 3);
        //materia de CNB
        listaMaterias.add("Matemática", "mate", 1);

        listaMaterias.add("Comunicación y Lenguaje, Idioma Español", "espanol", 3);
       // listaMaterias.add("Comunicación y Lenguaje, Idioma Extranjero", "extranjero", 4);
        listaMaterias.add("Ciencias Naturales", "naturales", 5);
        listaMaterias.add("Ciencias Sociales", "sociales", 6);
        listaMaterias.add("Educación Artística ", "artistica", 7);
        //listaMaterias.add("Emprendimiento para la Productividad", "emprendimiento", 8);
        //listaMaterias.add("Tecnologías del Aprendizaje y la Comunicación", "tac", 9);
        listaMaterias.add("Educación Física", "fisica", 10);
        listaMaterias.add("Otra", "mayas", 2);

        //grados

        //nivel_primario
        listaGrados.add("Primero primaria", "primero_43", 1);
        listaGrados.add("Segundo primaria", "segundo_43", 2);
        listaGrados.add("Tercero primaria", "tercero_43", 3);
        listaGrados.add("Cuarto primaria", "cuarto_43", 4);
        listaGrados.add("Quinto primaria", "quinto_43", 5);
        listaGrados.add("Sexto primaria", "sexto_43", 6);

        listaGrados.add("Primero básico", "primero_basico", 7);
        listaGrados.add("Segundo básico", "segundo_basico", 8);
        listaGrados.add( "Tercero básico", "tercero_basico", 9);

        //nivel preprimario 41-42
        listaGrados.add( "Grado único", "default_4142", 10);

        //nivel bachillerato 46
        listaGrados.add("Cuarto ", "primero_46", 11);
        listaGrados.add("Quinto", "segundo_46", 12);
        listaGrados.add( "Sexto", "tercero_46", 13);

    }



}
