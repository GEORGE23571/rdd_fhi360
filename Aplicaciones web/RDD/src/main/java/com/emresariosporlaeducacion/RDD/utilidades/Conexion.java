package com.emresariosporlaeducacion.RDD.utilidades;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class Conexion
{
   
    private static final String host="3.14.150.185:3306";
    private static final String user ="exe-admin";
    private static final String pass ="dr6studrlst_tra?iChuvicofaVozItRichiSpowikesifisuPricHixOphopiy5";
    private static final String db ="odk_prod";
    private  static      Connection con;
    
    private static Connection createDB()
    {
        
    try 
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://"+host+"/"+db, user, pass);
                return con;
         } 
        catch (Exception ex)
        {
            System.out.println("Database.getConnection() Error -->"+ ex.getMessage());
            return null;
        }
    }
            
    
    public static Connection getConnection() 
    {
        return createDB();
    }

    public static void close(Connection con) 
    {
        try 
        {
            con.close();		
        }
        catch (SQLException ex) 
        {
            System.out.println("erro al cerrar la conexion "+ ex.getMessage());
        }
    }
        
}
