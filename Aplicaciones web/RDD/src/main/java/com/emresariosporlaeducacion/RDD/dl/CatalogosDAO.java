package com.emresariosporlaeducacion.RDD.dl;

import com.emresariosporlaeducacion.RDD.modelo.*;
import com.emresariosporlaeducacion.RDD.utilidades.Conexion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;

public class CatalogosDAO 
{
     private static String error; 
     
     public static ArrayList<Departamento> getDepartamentosDAO()
    { 
        Connection con = null;
	PreparedStatement ps = null;
        ArrayList<Departamento> result=new  ArrayList ();
	try 
        {
            con = Conexion.getConnection();
            ps = con.prepareStatement( "select  *  from RDD_DEPARTAMENTO\n" +
                                        "WHERE COD_ESTADO =1 ;");
           
           
            ResultSet rs =ps.executeQuery();
           
            while(rs.next())
            {
                Departamento element = new Departamento();
                element.setId_depto(""+rs.getInt("ID_DEPARTAMENTO"));
                element.setNom_depto(rs.getString("NOMBRE"));
                result.add(element);
            }
        }
        
        catch (SQLException ex) 
        {
            error= ex.getMessage();
            return null;
        } 
        finally
        {
            Conexion.close(con);
        }
        return result;
    }
    
     public static ArrayList<Municipio> getMunicipiosDAO(int idDepto)
    { 
        Connection con = null;
	PreparedStatement ps = null;
        ArrayList<Municipio> result=new  ArrayList ();
	try 
        {
            con = Conexion.getConnection();
            ps = con.prepareStatement( "select * from RDD_MUNICIPIO\n" +
                                       " WHERE ID_DEPARTAMENTO =?  AND COD_ESTADO =1 ;");
            ps.setInt(1, idDepto);
           
            ResultSet rs =ps.executeQuery();
           
            while(rs.next())
            {
                Municipio element = new Municipio();
                element.setId_depto(rs.getInt("ID_DEPARTAMENTO"));
                element.setId_muni(rs.getInt("ID_MUNICIPIO"));
                element.setNombre_muni(rs.getString("NOMBRE"));
                result.add(element);
            }
        }
        catch (SQLException ex) 
        {
            error= ex.getMessage();
            return null;
        } 
        finally
        {
            Conexion.close(con);
        }
        return result;
    }
     
     public static ArrayList<Nivel> getNivelesDAO(Municipio muni)
    { 
        Connection con = null;
	PreparedStatement ps = null;
        ArrayList<Nivel> result=new  ArrayList ();
	try 
        {
            con = Conexion.getConnection();
            ps = con.prepareStatement( "select DISTINCT( A.ID_NIVEL),A.NOMBRE from RDD_NIVEL AS A \n" +
                                            "INNER JOIN RDD_ESTABLECIMIENTO AS B ON A.ID_NIVEL=B.ID_NIVEL\n" +
                                            "WHERE B.ID_DEPARTAMENTO =? AND B.ID_MUNICIPIO=?   AND COD_ESTADO =1;");
            ps.setInt(1, muni.getId_depto());
            ps.setInt(2, muni.getId_muni());
           
            ResultSet rs =ps.executeQuery();
           
            while(rs.next())
            {
                Nivel element = new Nivel();
                element.setId_nivel(rs.getInt("ID_NIVEL"));
                element.setNivel(rs.getString("NOMBRE"));
                result.add(element);
            }
        }
        catch (SQLException ex) 
        {
            error= ex.getMessage();
            return null;
        } 
        finally
        {
            Conexion.close(con);
        }
        return result;
    }
     
     public static ArrayList<Establecimiento> getEstablecimeintosDAO(Municipio muni)
    { 
        Connection con = null;
	PreparedStatement ps = null;
        ArrayList<Establecimiento> result=new  ArrayList ();
	try 
        {
            con = Conexion.getConnection();
            ps = con.prepareStatement( "select * from RDD_ESTABLECIMIENTO\n" +
                                        "WHERE ID_DEPARTAMENTO = ? AND ID_MUNICIPIO = ? ; ");
            ps.setInt(1, muni.getId_depto());
            ps.setInt(2, muni.getId_muni());
            ResultSet rs =ps.executeQuery();
           
            while(rs.next())
            {
                Establecimiento element = new Establecimiento();
                element.setCodigo(rs.getString("ID_DEPARTAMENTO")+"-"+rs.getString("ID_MUNICIPIO")+"-"+rs.getString("ID_ESTAB")+"-"+rs.getString("ID_NIVEL"));
                element.setNombre(rs.getString("NOMBRE"));
                element.setDireccion(rs.getString("DIRECCION"));
                element.setLatitud(rs.getDouble("LATITUD"));
                element.setLongitud(rs.getDouble("LONGITUD"));
                result.add(element);
            }
        }
        catch (SQLException ex) 
        {
            error= ex.getMessage();
            return null;
        } 
        finally
        {
            Conexion.close(con);
        }
        return result;
    }
     
     public static Establecimiento getEstablecimeintosDAO(String codigo)
    { 
        Connection con = null;
	PreparedStatement ps = null;
       Establecimiento element=new  Establecimiento();
	try 
        {
            con = Conexion.getConnection();
            ps = con.prepareStatement( "select * from RDD_ESTABLECIMIENTO\n" +
                                        "WHERE ID_DEPARTAMENTO = ? AND ID_MUNICIPIO = ?  AND ID_ESTAB = ? AND ID_NIVEL = ? ; ");
            String elements[]= codigo.split("-");
            
            ps.setInt(1,Integer.parseInt(elements[0]) );
            ps.setInt(2, Integer.parseInt(elements[1]));
            ps.setInt(3, Integer.parseInt(elements[2]));
            ps.setInt(4, Integer.parseInt(elements[3]));
            
            ResultSet rs =ps.executeQuery();
           
            while(rs.next())
            {
                 element = new Establecimiento();
                element.setCodigo(rs.getString("ID_DEPARTAMENTO")+"-"+rs.getString("ID_MUNICIPIO")+"-"+rs.getString("ID_ESTAB")+"-"+rs.getString("ID_NIVEL"));
                element.setNombre(rs.getString("NOMBRE"));
                element.setDireccion(rs.getString("DIRECCION"));
                element.setLatitud(rs.getDouble("LATITUD"));
                element.setLongitud(rs.getDouble("LONGITUD"));
                
            }
        }
        catch (SQLException ex) 
        {
            error= ex.getMessage();
            return null;
        } 
        finally
        {
            Conexion.close(con);
        }
        return element;
    }
     
    public static ErrorResponse getError() 
    {
        ErrorResponse excepcion= new ErrorResponse();
        excepcion.setMessage(error);
        return excepcion;
    }
    
    public static ArrayList<Parametro> getParametrosDAO()
    { 
        Connection con = null;
	PreparedStatement ps = null;
        ArrayList<Parametro> result=new  ArrayList ();
	try 
        {
            con = Conexion.getConnection();
            ps = con.prepareStatement( "SELECT * FROM RDD_PARAMETRO; ");
            ResultSet rs =ps.executeQuery();
           
            while(rs.next())
            {
                Parametro element = new Parametro(rs.getString("NOMBRE"),rs.getString("VALOR"));
                result.add(element);
            }
        }
        catch (SQLException ex) 
        {
            error= ex.getMessage();
            return null;
        } 
        finally
        {
            Conexion.close(con);
        }
        return result;
    }
     
    
}
