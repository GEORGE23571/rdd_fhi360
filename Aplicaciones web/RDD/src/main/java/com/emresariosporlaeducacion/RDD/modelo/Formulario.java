package com.emresariosporlaeducacion.RDD.modelo;


public class Formulario 
{
    private String nombre;
    private String id_form;
    private String body_xml;

    public Formulario(String nombre, String id_form, String body_xml) 
    {
        this.nombre = nombre;
        this.id_form = id_form;
        this.body_xml = body_xml;
    }

    public String getId_form() {
        return id_form;
    }

    public void setId_form(String id_form) {
        this.id_form = id_form;
    }

    public String getBody_xml() {
        return body_xml;
    }

    public void setBody_xml(String body_xml) {
        this.body_xml = body_xml;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
}
