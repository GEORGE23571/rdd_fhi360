package com.emresariosporlaeducacion.RDD.modelo;

public class Parametro
{
    
 private String name;
 private String valor;

    public String getName() {
        return name;
    }

    public String getValor() {
        return valor;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Parametro(String name, String valor) {
        this.name = name;
        this.valor = valor;
    }
 
}
