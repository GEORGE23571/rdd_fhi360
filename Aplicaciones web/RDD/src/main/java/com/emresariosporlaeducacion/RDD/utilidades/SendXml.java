package com.emresariosporlaeducacion.RDD.utilidades;

import com.emresariosporlaeducacion.RDD.modelo.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;



public class SendXml 

{
    private static String path_resources ="/APIS/RDD/resources_odk/";
    private static String Url_Server_ODK="http://3.14.150.185:8080/ODKAggregate";
                                          
    private static String jar_BriefCase="ODK-Briefcase-v1.17.4.jar";
    
     private static String userODK="admin";
     private static String passODK="1234";
    
    public static  void sendSumission(ArrayList<Reporte> lista)
     {
         String uri =java.util.UUID.randomUUID().toString(); 
         String path_carpeta_temporal=path_resources+uri;
         
         
         String path_Briefcase= path_carpeta_temporal+"/\"ODK Briefcase Storage\"/forms/\"Reporte de día de clases\"/instances";

         //realizar copia de estructura de ODK-BRIEFCASE
         RunCommand("mkdir "+ path_carpeta_temporal);
         
         RunCommand("cp -r "+ path_resources+"ODK_FILE/\"ODK Briefcase Storage\" "+path_carpeta_temporal+" ");
         
         //copia de archivo xml subbmission
         for (Reporte reporte:lista)
         {
             xmlForm e = new xmlForm();
             e.setReporte(reporte);
             RunCommand("mkdir "+  path_Briefcase+"/"+reporte.getUri());
             e.saveXML(path_resources+reporte.getUri()+".xml");
             RunCommand("mv "+path_resources+reporte.getUri()+".xml "+  path_Briefcase+"/"+reporte.getUri()+"/submission.xml");
      
         }
         
         //envio de lote de respuestas al servidor ODK
         RunCommand("java -jar "+path_resources+jar_BriefCase+" -psha -sd "+path_carpeta_temporal+" --odk_url "+Url_Server_ODK+" --odk_username "+userODK+" --odk_password "+passODK );
         System.out.println("java -jar "+path_resources+jar_BriefCase+" -psha -sd "+path_carpeta_temporal+" --odk_url "+Url_Server_ODK+" --odk_username "+userODK+" --odk_password "+passODK);
         RunCommand("rm -r "+path_carpeta_temporal+" ");
         RunCommand("clear");
         
     }

    public static String getPath_resources() {
        return path_resources;
    }


    private static void RunCommand(String comando)
    {
        Process p;
        
        String[] cmd = {"/bin/bash","-c",comando};

        
        try 
        {
             
            p = Runtime.getRuntime().exec(cmd);
            System.out.println("Comando iniciado.");
            Escribir.escribir("\n\n Comando Iniciado ");
            boolean no_exit = true;
            while(no_exit)
            {
                try 
                {
                    p.exitValue();
                    no_exit = false;
                }
                catch(IllegalThreadStateException exception)
                {
                    no_exit=true;
                    
                }
             }
            
            System.out.println("Comando finalizado.");
            Escribir.escribir("Comando finalizado "+comando);
            InputStreamReader entrada = new InputStreamReader(p.getInputStream());
            BufferedReader stdInput = new BufferedReader(entrada);
           
            String salida;
            //Si el comando tiene una salida la mostramos
            if((salida=stdInput.readLine()) != null)
            {
            	System.out.println("Comando ejecutado correctamente");
                
            	while ((salida=stdInput.readLine()) != null)
                {
                	System.out.println(salida);
                         Escribir.escribir(salida);
                }
            }
            else
            {
                Escribir.escribir("No se a producido ninguna salida");
            	System.out.println("No se a producido ninguna salida");
            }
        }
        catch (IOException ex)
        {
            Logger.getLogger(SendXml.class.getName()).log(Level.SEVERE, null, ex);
            Escribir.escribir("\n ***************fallo comando  \n");
            Escribir.escribir(ex.getMessage());
        }
    }
}
