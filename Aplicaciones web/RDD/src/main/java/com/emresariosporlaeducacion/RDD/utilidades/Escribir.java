package com.emresariosporlaeducacion.RDD.utilidades;



import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


public class Escribir
{

    
    public static void escribir(String cont)
    {

    BufferedWriter bw = null;
    FileWriter fw = null;

    try 
    {
        File file = new File("/APIS/RDD/resources_odk/log.txt");
        // Si el archivo no existe, se crea!
        if (!file.exists()) {
            file.createNewFile();
        }
        // flag true, indica adjuntar información al archivo.
        fw = new FileWriter(file.getAbsoluteFile(), true);
        bw = new BufferedWriter(fw);
        bw.write(cont+"\n");
    } catch (IOException e) {
        e.printStackTrace();
    } finally 
    {
        try {
                        //Cierra instancias de FileWriter y BufferedWriter
            if (bw != null)
                bw.close();
            if (fw != null)
                fw.close();
        } catch (IOException ex) 
        {
            ex.printStackTrace();
        }
    }
    
    }
  
}
