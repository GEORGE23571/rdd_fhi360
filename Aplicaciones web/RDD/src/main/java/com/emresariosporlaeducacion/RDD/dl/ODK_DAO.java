
package com.emresariosporlaeducacion.RDD.dl;

import com.emresariosporlaeducacion.RDD.modelo.*;
import com.emresariosporlaeducacion.RDD.utilidades.Conexion;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class ODK_DAO 
{
     private static String error; 
     private static String form_id = "dia_de_clases_3";
     
    public static ArrayList<Respuesta> getRespuestasDAO(ArrayList<Reporte> reportes)
    { 
        Connection con = null;
	PreparedStatement ps = null;
        ArrayList<Respuesta> result=new  ArrayList ();
        
	try 
        {
            con = Conexion.getConnection();
            
            String uris_parametros ="";

            for(int i =0 ; i< reportes.size()-1;i++)
            {
                uris_parametros=uris_parametros+"?,";
            }
            
            uris_parametros=uris_parametros+"?";
            
            
            ps = con.prepareStatement( "SELECT _URI  FROM "+form_id.toUpperCase()+"_CORE where _URI in ("+uris_parametros+");");
            
            for(int i =0 ; i< reportes.size();i++)
            {
                 ps.setString(i+1, "uuid:"+reportes.get(i).getUri());
            }
            
            ResultSet rs =ps.executeQuery();
           
            ArrayList<String> uris_odk = new ArrayList();
            
            while(rs.next())
            {
                uris_odk.add(rs.getString("_URI"));
            }
            
            
            for(int i =0 ; i< reportes.size();i++)
            {
                
                Respuesta element = new Respuesta();
                element.setUri(reportes.get(i).getUri());
                
                if(uris_odk.contains("uuid:"+reportes.get(i).getUri()))
                {
                     element.setEstado(1);
                }
                else
                {
                    element.setEstado(0);
                }
                
                result.add(element);
            }
        }
        catch (SQLException ex) 
        {
            error= ex.getMessage();
            System.out.println("wwerwer "+ex);
            return null;
        } 
        finally
        {
            Conexion.close(con);
        }
        
        return result;
    }
    
    
    public static Usuario getUsuario(Usuario user)
    {
        Connection con = null;
	PreparedStatement ps = null;
        user.setStatus(false);
	try 
        {
            con = Conexion.getConnection();
            
            ps = con.prepareStatement( "SELECT * FROM usuarioReportes A \n" +
                                       "INNER JOIN rolReportes B on A.idRol= B.idRol " +
                                       "WHERE A.password = ? AND A.usuario = ? ");
            
            ps.setString(1,user.getPassword() );
            ps.setString(2, user.getUser());
            ResultSet rs =ps.executeQuery();
           
            while(rs.next())
            {
                user.setNom_user(rs.getString("nombre"));
                user.setUser(rs.getString("usuario"));
                user.setRol(rs.getString("nombreRol"));
                user.setStatus(true);
              return user;
            }
            
        }
        catch (SQLException ex) 
        {
            error= ex.getMessage();
            System.out.println("error "+ error);
            
        } 
        finally
        {
            Conexion.close(con);
        }
        user.setMessage("Autenticación fallida. Verifique usuario y contraseña");
        return user;
    }
    
    
    public static ArrayList<Formulario> getFormulariosDAO()
    { 
        Connection con = null;
	PreparedStatement ps = null;
        ArrayList<Formulario> result=new  ArrayList ();
        
	try 
        {
            con = Conexion.getConnection();
            ps = con.prepareStatement( "SELECT * FROM RDD_FORMULARIO order by nombre desc ;");
            
            ResultSet rs =ps.executeQuery();
           
            
            while(rs.next())
            {
                result.add(new Formulario(rs.getString("NOMBRE"),rs.getString("ID_FORM"),rs.getString("BODY_XML")));
            }
            
        }
        catch (SQLException ex) 
        {
            error= ex.getMessage();
            return null;
        } 
        finally
        {
            Conexion.close(con);
        }
        
        return result;
    }
    
    public static String getFormID()
     {
         
      return form_id;
     }
    
    
       public static ErrorResponse getError() 
    {
        ErrorResponse excepcion= new ErrorResponse();
        excepcion.setMessage(error);
        
        return excepcion;
    }


    
}
