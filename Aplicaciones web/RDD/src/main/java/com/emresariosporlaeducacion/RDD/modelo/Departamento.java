package com.emresariosporlaeducacion.RDD.modelo;


public class Departamento
{
    private String id_depto;
    private String nom_depto;

    public void setId_depto(String id_depto) {
        this.id_depto = id_depto;
    }

    public void setNom_depto(String nom_depto) {
        this.nom_depto = nom_depto;
    }

    public String getId_depto() {
        return id_depto;
    }

    public String getNom_depto() {
        return nom_depto;
    }
    
    

}
