package com.emresariosporlaeducacion.RDD;


import com.emresariosporlaeducacion.RDD.dl.CatalogosDAO;
import com.emresariosporlaeducacion.RDD.dl.ODK_DAO;
import com.emresariosporlaeducacion.RDD.modelo.*;
import com.emresariosporlaeducacion.RDD.utilidades.SendXml;
import com.google.gson.Gson;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/wsApp")
public class ServiceController
{
    @GetMapping("Hola")
    public String saludar()
    {

        return "Hello word";
    }

    @GetMapping("/getDepartamentos")
    public ResponseEntity<String> getDepartamentos()
    {

        List<Departamento> element = CatalogosDAO.getDepartamentosDAO();
        Gson gson = new Gson();
        if (element ==null)
        {
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
        else
        {
            String jsonRes = gson.toJson(element);
            jsonRes ="{\"departamentos\":"+jsonRes +" }" ;
            return new ResponseEntity<>(jsonRes, HttpStatus.OK);
        }
    }


    @PostMapping("/getMunicipios")
    public ResponseEntity<String> getMunicipios(@RequestBody Departamento depa)
    {

        List<Municipio> element = CatalogosDAO.getMunicipiosDAO(Integer.parseInt(depa.getId_depto()));

        if (element ==null)
        {
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
        else
        {
            Gson gson = new Gson();
            String jsonRes = gson.toJson(element);
            jsonRes ="{\"municipios\":"+jsonRes +" }" ;
            return new ResponseEntity<>(jsonRes, HttpStatus.OK);
        }
    }

    @PostMapping("/getNiveles")
    public ResponseEntity<String> getNiveles(@RequestBody Municipio muni)
    {

        List<Nivel> element =CatalogosDAO.getNivelesDAO(muni);

        if (element ==null)
        {
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
        else
        {
            Gson gson = new Gson();
            String jsonRes = gson.toJson(element);
            jsonRes ="{\"niveles\":"+jsonRes +" }" ;
            return new ResponseEntity<>(jsonRes, HttpStatus.OK);
        }
    }

    @PostMapping("/getCentros")
    public ResponseEntity<List<Establecimiento>> getCentros(@RequestBody Municipio muni)
    {

        List<Establecimiento> element =CatalogosDAO.getEstablecimeintosDAO(muni);

        if (element ==null)
        {
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>(element, HttpStatus.OK);
        }
    }

    @PostMapping("/setReporte")
    public ResponseEntity<List<Respuesta>> getCentros(@RequestBody ArrayList<Reporte>  lista )
    {
        SendXml.sendSumission(lista);
        ArrayList<Respuesta> element = ODK_DAO.getRespuestasDAO(lista);

        if (element ==null)
        {
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>(element, HttpStatus.OK);
        }
    }

    @GetMapping("/getFormularios")
    public ResponseEntity<List<Formulario>> getFormularios()
    {

        List<Formulario> element =  ODK_DAO.getFormulariosDAO();

        if (element ==null)
        {
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>(element, HttpStatus.OK);
        }
    }
    @GetMapping("/getParametros")
    public ResponseEntity<String> getTel()
    {
        Object  element =  CatalogosDAO.getParametrosDAO();

        if (element ==null)
        {
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
        else
        {
            Gson gson = new Gson();
            String jsonRes = gson.toJson(element);
            //.jsonRes ="{\"parametros\":[{ \"name\":\"53335741\" ,  \"valor\":"+jsonRes +"} ]}";
            return new ResponseEntity<>(jsonRes, HttpStatus.OK);
        }
    }
}
