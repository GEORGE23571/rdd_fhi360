package com.emresariosporlaeducacion.RDD.modelo;


public class Usuario
{
    private String user;
    private String nom_user;
    private String password;
    private String rol;
    private String message;
    private boolean status;
    
    
    
    public String getMessage() {
        return message;
    }
    
    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setNom_user(String nom_user) {
        this.nom_user = nom_user;
    }

    public String getNom_user() {
        return nom_user;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
    
    
}
