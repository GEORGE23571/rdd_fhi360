package com.emresariosporlaeducacion.RDD.modelo;


import com.emresariosporlaeducacion.RDD.dl.CatalogosDAO;
import com.emresariosporlaeducacion.RDD.utilidades.Escribir;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;



public class Reporte 
{

    private String centro;
    private String actividad="";
    private String fecha;
    private String grado;
    private String time_start;
    private String time_end;
    private String uri;
    private String materias;
    private String longitud;
    private String latitud;
    
    
    public String getIdDepto()
    {
     return centro.substring(0, 2);
    }
    
    public String getIdMun()
    {
        return centro.substring(3, 5);
    }
    
    public String getGeopiont()
    {
        String Coordenadas="";

        if (latitud==null || longitud==null||latitud.trim().equals("")||longitud.trim().equals(""))
        {
           Establecimiento e = CatalogosDAO.getEstablecimeintosDAO(this.centro);
           Coordenadas=e.getLatitud()+" "+  e.getLongitud()+" 0 0";
           
        }
        else
        {
          Coordenadas=latitud+" "+  longitud+" 0 0";
        }
        Escribir.escribir(this.centro+"codifg de centro a busca "+ Coordenadas);
        return Coordenadas;
    }
    
    public void setUri(String uri) 
    {
        this.uri = uri;
    }
    public String getCentro() {
        return centro;
    }

    public String getActividad() 
    {
        return actividad;
    }

    public String getFecha() 
    { 
        
        return fecha;
    }

    public String getGrado() {
        return grado;
    }

    public String getTime_start() {
        return time_start;
    }

    public String getTime_end() {
        return time_end;
    }

    public String getMaterias() {
        return materias;
    }

    public void setCentro(String centro) {
        this.centro = centro;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public void setFecha(String fecha) {
  
        
        this.fecha = fecha;
        
    }

    public void setGrado(String grado) {
        this.grado = grado;
    }

    public void setTime_start(String time_start) {
        this.time_start = time_start;
    }

    public void setTime_end(String time_end) {
        this.time_end = time_end;
    }

    public void setMaterias(String materias) {
        this.materias = materias;
    }

    public String getUri() {
        return uri;
    }

    public String getLongitud() {
        return longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

}
