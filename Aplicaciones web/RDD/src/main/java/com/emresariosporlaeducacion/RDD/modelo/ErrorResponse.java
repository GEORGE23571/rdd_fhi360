package com.emresariosporlaeducacion.RDD.modelo;


public class ErrorResponse
{

    
    private int estatus =0;
    private String message ="";
    
    public String getMessage() {
        return message;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setEstats(int estatus) {
        this.estatus = estatus;
    }
    
    
}
