package com.emresariosporlaeducacion.RDD.modelo;


public class Municipio 
{
    private int id_muni;
    private int id_depto;
    private String nombre_muni;

    public int getId_muni() {
        return id_muni;
    }

    public int getId_depto() {
        return id_depto;
    }

    public String getNombre_muni() {
        return nombre_muni;
    }

    public void setId_muni(int id_muni) {
        this.id_muni = id_muni;
    }

    public void setId_depto(int id_depto) {
        this.id_depto = id_depto;
    }

    public void setNombre_muni(String nombre_muni) {
        this.nombre_muni = nombre_muni;
    }
    
    
}
