package com.emresariosporlaeducacion.RDD.modelo;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.emresariosporlaeducacion.RDD.dl.ODK_DAO;
import com.emresariosporlaeducacion.RDD.utilidades.*;
import org.jdom2.*;
import org.jdom2.filter.ElementFilter;
import org.jdom2.filter.Filter;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;


public class xmlForm 
{
     private SAXBuilder saxBuilder ;
     private String form_id ;
     private String version = "0";
     private Document document;
     private Namespace nsHeader = Namespace.getNamespace("http://opendatakit.org/submissions");
     private Namespace nsFooter = Namespace.getNamespace("http://openrosa.org/xforms");
     private String path_resources = SendXml.getPath_resources();

     
     public xmlForm()
     {
         File inputFile = new File(path_resources+"xmlFormSubmission.xml");
         saxBuilder = new SAXBuilder();
         form_id= ODK_DAO.getFormID();

         try 
         {
           document = saxBuilder.build(inputFile);
          
           document.getRootElement().setAttribute("id",this.form_id);
           document.getRootElement().setAttribute("version",this.version);
           
         } 
         catch (JDOMException | IOException ex) 
         {
             Logger.getLogger(xmlForm.class.getName()).log(Level.SEVERE, null, ex);
             System.out.println(""+ex);
             Escribir.escribir(ex.toString());
         }
     }
     

     
     public void setReporte(Reporte reporte)
     {
         try
         {
               document.getRootElement().setAttribute("instanceID","uuid:"+ reporte.getUri());
               document.getRootElement().getChild("meta",nsFooter).getChild("instanceID",nsFooter).setText("uuid:"+reporte.getUri());
               document.getRootElement().getChild("centro_group",nsHeader).getChild("centro",nsHeader).setText(reporte.getCentro());
               document.getRootElement().getChild("depto_mun",nsHeader).getChild("departamento",nsHeader).setText(reporte.getIdDepto());
               document.getRootElement().getChild("depto_mun",nsHeader).getChild("municipio",nsHeader).setText(reporte.getIdMun());
               document.getRootElement().getChild("fecha_group",nsHeader).getChild("fecha",nsHeader).setText(reporte.getFecha());
               document.getRootElement().getChild("actividad_group",nsHeader).getChild("actividad",nsHeader).setText(reporte.getActividad());
               document.getRootElement().getChild("geopoint_widget",nsHeader).setText(reporte.getGeopiont());
               
              if (reporte.getActividad().equals("no_se_impartieron_materias"))
               {
                    document.getRootElement().getChild("materias_perdidas",nsHeader).getChild("grados",nsHeader).setText(reporte.getGrado());

                     document.getRootElement().getChild("materias_perdidas",nsHeader).getChild("materias",nsHeader).setText(reporte.getMaterias());
               }
         }
         catch(Exception e )
         {
             System.out.println("error--->  "+e);
             Escribir.escribir(e.toString());
         }
     }
     
     public void saveXML( String path)
     {
         
         Format format = Format.getPrettyFormat();  
         XMLOutputter xmloutputter = new XMLOutputter(format);  
         String docStr = xmloutputter.outputString(document);
         
         try 
         {
           File output = new File(path);
            FileWriter writer = new FileWriter(output);

            writer.write(docStr);
            writer.flush();
            writer.close();
            
         }
         catch (IOException ex)
         {
             Logger.getLogger(xmlForm.class.getName()).log(Level.SEVERE, null, ex);
             System.out.println("error al escribir xml");
             Escribir.escribir(ex.toString());
         }
     }
}
